package com.marchenko.news.controller;


import com.marchenko.news.entity.*;
import com.marchenko.news.exception.ServiceException;
import com.marchenko.news.service.interfaces.AuthorAction;
import com.marchenko.news.service.interfaces.NewsAction;
import com.marchenko.news.service.interfaces.NewsPostAction;
import com.marchenko.news.service.interfaces.TagAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by Marchenko Vadim on 4/5/2016.
 */

@Controller
public class NewsController {
    private static final String KEY_NEWS = "news";
    private static final String KEY_COMMENTS = "comments";
    private static final String KEY_NEWS_POST = "post";
    private static final String KEY_AUTHORS_LIST = "authorList";
    private static final String KEY_NEWS_AUTHOR = "newsAuthor";
    private static final String KEY_TAGS = "tags";
    private static final String KEY_POST_TAGS = "actualTags";
    private static final String KEY_NON_POST_TAGS = "otherTags";
    private static final String PAGE_NEWS_VIEW = "news-view";
    private static final String PAGE_NEWS_EDITOR = "news-editor";
    private static final String PAGE_NEWS_CREATOR = "news-creator";
    private static final String REDIRECT_TO_NEWS_EDITOR_PAGE = "redirect:/news-editor?id=";
    private static final String REDIRECT_TO_NEWS_VIEW_PAGE = "redirect:/view-news?id=";
    private static final String REDIRECT_TO_NEWS_CREATOR_PAGE = "redirect:/news-creator";
    private static final String KEY_VALIDATION_ERROR = "org.springframework.validation.BindingResult.";

    @Autowired
    private NewsPostAction newsPostAction;
    @Autowired
    private AuthorAction authorAction;
    @Autowired
    private TagAction tagAction;
    @Autowired
    private NewsAction newsAction;

    @RequestMapping("/view-news")
    public String showNewsViewPage(@RequestParam Long id, Map<String, Object> model) throws ServiceException {
        NewsPost newsPost = newsPostAction.readNews(id).get();

        List<Comment> comments = newsPost.getComments();
        comments.sort((a, b) -> a.getCreationDate()
                .compareTo(b.getCreationDate()));

        model.put(KEY_NEWS_POST, newsPost);
        return PAGE_NEWS_VIEW;
    }

    @RequestMapping("/news-editor")
    public String showNewsEditorPage(@RequestParam Long id, Map<String, Object> model) throws ServiceException {
        NewsPost newsPost = newsPostAction.readNews(id).get();
        List<Comment> postComments = newsPost.getComments();
        List<Author> nonExpiredAuthors = authorAction.findNonExpiredAuthors();
        List<Tag> postTags = newsPost.getTags();
        List<Tag> nonPostTags = tagAction.findAll();
        nonPostTags.removeAll(postTags);

        postTags.sort((a, b) -> a.getTagName().compareTo(b.getTagName()));
        nonPostTags.sort((a, b) -> a.getTagName().compareTo(b.getTagName()));
        nonExpiredAuthors.sort((a, b) -> a.getName().compareTo(b.getName()));
        postComments.sort((a, b) -> a.getCreationDate().compareTo(b.getCreationDate()));

        model.put(KEY_NEWS_AUTHOR, newsPost.getAuthor());
        model.put(KEY_AUTHORS_LIST, nonExpiredAuthors);
        model.put(KEY_POST_TAGS, postTags);
        model.put(KEY_NON_POST_TAGS, nonPostTags);
        model.put(KEY_COMMENTS, postComments);

        if (!model.containsKey(KEY_NEWS)) {
            model.put(KEY_NEWS, newsPost.getNews());
        }

        return PAGE_NEWS_EDITOR;
    }

    @RequestMapping(value = "/edit-news", method = RequestMethod.POST)
    public String editNews(@RequestParam Long authorId,
                           @RequestParam(required = false) Long[] tagsId,
                           @Valid @ModelAttribute News newsAttribute,
                           BindingResult result,
                           RedirectAttributes attributes) throws ServiceException {
        NewsPost newsPost = new NewsPost();

        if (result.hasErrors()) {
            attributes.addFlashAttribute( KEY_VALIDATION_ERROR +
                    KEY_NEWS, result);
            attributes.addFlashAttribute(KEY_NEWS, newsAttribute);
            return REDIRECT_TO_NEWS_EDITOR_PAGE + newsAttribute.getId();
        }

        updateDateFieldsInNews(newsAttribute);
        Author author = authorAction.readAuthor(authorId).get();
        List<Tag> tags = new ArrayList<>();

        if (tagsId != null) {
            tags = tagAction.readSequence(Arrays.asList(tagsId));
        }

        newsPost.setNews(newsAttribute);
        newsPost.setAuthor(author);
        newsPost.setTags(tags);

        newsPostAction.editNewsPostContent(newsPost);

        return REDIRECT_TO_NEWS_VIEW_PAGE + newsAttribute.getId();
    }

    @RequestMapping("/news-creator")
    public String showNewsCreator(Map<String, Object> model) throws ServiceException {
        List<Author> authors = authorAction.findNonExpiredAuthors();
        List<Tag> allTags = tagAction.findAll();

        model.put(KEY_TAGS, allTags);
        model.put(KEY_AUTHORS_LIST, authors);

        if (!model.containsKey(KEY_NEWS)) {
            model.put(KEY_NEWS, new News());
        }

        return PAGE_NEWS_CREATOR;
    }

    @RequestMapping("/create-news")
    public String createNewsMessage(@RequestParam Long authorId, @RequestParam(required = false) Long[] tagsId,
                                    @Valid @ModelAttribute News newsAttribute, BindingResult result,
                                    RedirectAttributes attributes) throws ServiceException {
        NewsPost newsPost = new NewsPost();

        if (result.hasErrors()) {
            attributes.addFlashAttribute(KEY_VALIDATION_ERROR + KEY_NEWS, result);
            attributes.addFlashAttribute(KEY_NEWS, newsAttribute);
            return REDIRECT_TO_NEWS_CREATOR_PAGE;
        }

        newsAttribute.setCreationDate(LocalDateTime.now());
        newsAttribute.setModificationDate(LocalDateTime.now());

        Author author = authorAction.readAuthor(authorId).get();
        List<Tag> tags = new ArrayList<>();
        if (tagsId != null) {
            tags = tagAction.readSequence(Arrays.asList(tagsId));
        }

        newsPost.setAuthor(author);
        newsPost.setNews(newsAttribute);
        newsPost.setTags(tags);

        Long id = newsPostAction.createNewsPost(newsPost);

        return REDIRECT_TO_NEWS_VIEW_PAGE + id;
    }

    private void updateDateFieldsInNews(News newNews) throws ServiceException {
        News oldNews = newsAction.read(newNews.getId()).get();
        newNews.setModificationDate(LocalDateTime.now());
        newNews.setCreationDate(oldNews.getCreationDate());
    }
}
