package com.marchenko.news.controller;

import com.marchenko.news.exception.ServiceException;
import com.marchenko.news.service.interfaces.NewsAction;
import com.marchenko.news.service.interfaces.UserAction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * Created by Marchenko Vadim on 3/29/2016.
 */

@Controller
public class LoginController {

    @RequestMapping("/")
    public String showHomePage(@RequestParam(required = false) String type,
                               Map<String, Object> model) throws ServiceException {
        if (type != null && "error".equals(type)){
            model.put("error", "Invalid username or password");
        }
        return "login";
    }
}