package com.marchenko.news.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Marchenko Vadim on 5/4/2016.
 */

@ControllerAdvice
public class ErrorHandlerController {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final String PAGE_ERROR = "error_page";

    @ExceptionHandler(Exception.class)
    public String showErrorPageHandleException(Exception exception){
        LOGGER.error("Catching exception: ",exception);
        return PAGE_ERROR;
    }

    @RequestMapping("/403")
    public String showHomePage(){
        return "403";
    }
}
