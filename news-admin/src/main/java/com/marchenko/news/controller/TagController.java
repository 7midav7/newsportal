package com.marchenko.news.controller;

import com.marchenko.news.entity.Tag;
import com.marchenko.news.exception.ServiceException;
import com.marchenko.news.service.interfaces.TagAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

/**
 * Created by Marchenko Vadim on 4/20/2016.
 */

@Controller
public class TagController {
    private static final long VALUE_FOR_MARK_INVALID_TAG_IN_UPDATING = 1_000_000_000_000_000L;
    private static final String KEY_TAG_LIST = "tags";
    private static final String PAGE_TAG_LIST = "tags";

    @Autowired
    private TagAction tagAction;

    @RequestMapping(value = {"/update-tag"}, method = RequestMethod.POST)
    public @ResponseBody Tag updateTag(@Valid @ModelAttribute Tag tag,
                                       BindingResult result) throws ServiceException {
        if (result.hasErrors()){
            String message = result.getAllErrors().get(0).getDefaultMessage();
            tag.setId(VALUE_FOR_MARK_INVALID_TAG_IN_UPDATING + tag.getId());
            tag.setTagName(message);
        } else {
            tagAction.updateTag(tag);
        }
        return tag;
    }

    @RequestMapping(value = {"/delete-tag"}, method = RequestMethod.POST)
    public @ResponseBody String deleteTag(@RequestParam String id) throws ServiceException {
        tagAction.deleteTag(Long.valueOf(id));
        return id;
    }

    @RequestMapping(value = {"/create-tag"}, method = RequestMethod.POST)
    public @ResponseBody Tag createTag(@Valid @ModelAttribute Tag tag,
                                       BindingResult result) throws ServiceException {
        if (result.hasErrors()) {
            String message = result.getAllErrors().get(0).getDefaultMessage();
            tag.setTagName(message);
        } else {
            tagAction.createTag(tag);
        }
        return tag;
    }

    @RequestMapping(value = {"/tags"})
    public String showHomePage(Map<String, Object> model) throws ServiceException {
        model.put(KEY_TAG_LIST, tagAction.findAll());
        return PAGE_TAG_LIST;
    }
}
