package com.marchenko.news.controller;

import com.marchenko.news.entity.*;
import com.marchenko.news.exception.ServiceException;
import com.marchenko.news.service.interfaces.AuthorAction;
import com.marchenko.news.service.interfaces.NewsAction;
import com.marchenko.news.service.interfaces.NewsPostAction;
import com.marchenko.news.service.interfaces.TagAction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * Created by Marchenko Vadim on 4/30/2016.
 */

@Controller
public class NewsListController {
    private static final long NUMBER_NEWS_ON_PAGE = 3;
    private static final String KEY_NEWS_POST_LIST = "newses";
    private static final String KEY_NUMBER_CURRENT_PAGE = "currentPage";
    private static final String KEY_PAGE_COUNT = "pageCount";
    private static final String KEY_SELECTED_AUTHOR_IN_FILTER = "filterAuthor";
    private static final String KEY_SELECTED_TAGS_IN_FILTER = "filterTags";
    private static final String KEY_NON_SELECTED_TAGS_IN_FILTER = "otherTags";
    private static final String KEY_AUTHOR_LIST = "authors";
    private static final String KEY_FILTER_URI = "filterUri";
    private static final String PAGE_NEWS_LIST = "newses";
    private static final String REDIRECT_NEWS_LIST_PAGE = "redirect:/newses?page=1";
    private static final String NAME_AUTHOR_ID_PARAMETER = "authorId";
    private static final String NAME_TAG_ID_LIST_PARAMETER = "tagIds";

    @Autowired
    private AuthorAction authorAction;
    @Autowired
    private TagAction tagAction;
    @Autowired
    private NewsPostAction newsPostAction;
    @Autowired
    private NewsAction newsAction;

    @ModelAttribute
    public SearchCriteria createCriteria(@RequestParam(required = false) Long authorId,
                                         @RequestParam(required = false) Long[] tagIds) {
        if (authorId == null && tagIds == null) {
            return null;
        }

        SearchCriteria searchCriteria = new SearchCriteria();

        searchCriteria.setAuthorId(authorId);
        if (tagIds != null) {
            searchCriteria.setTagIdList(Arrays.asList(tagIds));
        }

        return searchCriteria;
    }

    @RequestMapping("/newses")
    public String showNewsListPage(@RequestParam(required = false) Long page,
                                   Map<String, Object> model,
                                   @ModelAttribute SearchCriteria searchCriteria) throws ServiceException {
        if (page == null) {
            page = 1L;
        }

        Long firstNewsIndex = NUMBER_NEWS_ON_PAGE * (page - 1) + 1;
        Long lastNewsIndex = NUMBER_NEWS_ON_PAGE * page;

        putFilterAttributesInModel(model, searchCriteria);
        putPageContentInModel(model, searchCriteria, firstNewsIndex, lastNewsIndex);

        model.put(KEY_NUMBER_CURRENT_PAGE, page);

        return PAGE_NEWS_LIST;
    }

    @RequestMapping(value = "/filter-on")
    public String switchOnFilter(@ModelAttribute SearchCriteria searchCriteria)
            throws ServiceException {
        String filterUri = makeFilterUri(searchCriteria);

        return REDIRECT_NEWS_LIST_PAGE + filterUri;
    }

    @RequestMapping(value = "/filter-off")
    public String switchOffFilter() throws ServiceException {
        return REDIRECT_NEWS_LIST_PAGE;
    }

    @RequestMapping(value = "delete-newses")
    public String deleteNewses(@RequestParam(required = false) Long[] newsIdList) throws ServiceException {
        if (newsIdList != null) {
            for (int i = 0; i < newsIdList.length; ++i) {
                newsPostAction.deleteNewsMessage(newsIdList[i]);
            }
        }
        return REDIRECT_NEWS_LIST_PAGE;
    }

    private void putPageContentInModel(Map<String, Object> model, SearchCriteria searchCriteria,
                                       Long firstIndex, Long lastIndex) throws ServiceException {
        List<News> newses;
        Long pageCount;
        if (searchCriteria != null) {
            newses = newsAction.findBySearchCriteria(searchCriteria, firstIndex, lastIndex);
            pageCount = newsAction.countNewsesBySearchCriteria(searchCriteria);
        } else {
            newses = newsAction.takeMostCommentedNews(firstIndex, lastIndex);
            pageCount = newsAction.countNewses();
        }

        List<NewsPost> posts = newsPostAction.convert(newses);
        model.put(KEY_NEWS_POST_LIST, posts);

        pageCount = (pageCount + NUMBER_NEWS_ON_PAGE - 1) / NUMBER_NEWS_ON_PAGE;
        model.put(KEY_PAGE_COUNT, pageCount);
    }

    private void putFilterAttributesInModel(Map<String, Object> model,
                                            SearchCriteria searchCriteria) throws ServiceException {
        List<Author> allAuthors = authorAction.findNonExpiredAuthors();
        List<Tag> nonSearchingTags = tagAction.findAll();
        List<Tag> searchingTags = new ArrayList<>();

        Long authorId = takeAuthorAndTagsFromCriteria(searchCriteria, searchingTags);
        nonSearchingTags.removeAll(searchingTags);

        searchingTags.sort((a, b) -> a.getTagName().compareTo(b.getTagName()));
        nonSearchingTags.sort((a, b) -> a.getTagName().compareTo(b.getTagName()));

        String filterUri = makeFilterUri(searchCriteria);

        model.put(KEY_AUTHOR_LIST, allAuthors);
        model.put(KEY_SELECTED_AUTHOR_IN_FILTER, authorId);
        model.put(KEY_SELECTED_TAGS_IN_FILTER, searchingTags);
        model.put(KEY_NON_SELECTED_TAGS_IN_FILTER, nonSearchingTags);
        model.put(KEY_FILTER_URI, filterUri);
    }

    private Long takeAuthorAndTagsFromCriteria(SearchCriteria searchCriteria, List<Tag> resultTags) throws ServiceException {
        if (searchCriteria != null) {
            List<Long> tagIdList = searchCriteria.getTagIdList();
            if (tagIdList != null) {
                resultTags.addAll(tagAction.readSequence(tagIdList));
            }
            return searchCriteria.getAuthorId();
        } else {
            return null;
        }
    }

    private String makeFilterUri(SearchCriteria searchCriteria) {

        if (searchCriteria == null){
            return "";
        }

        StringBuilder filterUri = new StringBuilder();
        Long authorId = searchCriteria.getAuthorId();
        if (authorId != null) {
            filterUri.append("&").append(NAME_AUTHOR_ID_PARAMETER).append("=").append(authorId);
        }
        List<Long> tagIdList = searchCriteria.getTagIdList();
        if (tagIdList != null) {
            for (Long tagId : tagIdList) {
                filterUri.append("&").append(NAME_TAG_ID_LIST_PARAMETER).append("=").append(tagId);
            }
        }
        return filterUri.toString();
    }
}
