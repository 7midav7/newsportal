package com.marchenko.news.controller;

import com.marchenko.news.entity.Author;
import com.marchenko.news.exception.ServiceException;
import com.marchenko.news.service.interfaces.AuthorAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * Created by Marchenko Vadim on 4/20/2016.
 */

@Controller
public class AuthorController {
    private static final long VALUE_FOR_MARK_INVALID_TAG_IN_UPDATING = 1_000_000_000_000_000L;
    private static final String PAGE_AUTHORS_LIST = "authors";
    private static final String KEY_AUTHORS_LIST = "authors";
    @Autowired
    private AuthorAction authorAction;

    @RequestMapping({"/authors"})
    public String showAuthorListPage(Map<String, Object> model) throws ServiceException {
        List<Author> authorList = authorAction.findNonExpiredAuthors();
        model.put(KEY_AUTHORS_LIST, authorList);
        return PAGE_AUTHORS_LIST;
    }

    @RequestMapping(value = {"/update-author"}, method = RequestMethod.POST)
    public @ResponseBody Author updateAuthor(@Valid @ModelAttribute Author author,
                                             BindingResult validationResult) throws ServiceException {
        if (validationResult.hasErrors()){
            String errorMessage = validationResult.getAllErrors().get(0).getDefaultMessage();
            author.setId(VALUE_FOR_MARK_INVALID_TAG_IN_UPDATING + author.getId());
            author.setName(errorMessage);
        } else {
            authorAction.updateAuthor(author);
        }
        return author;
    }

    @RequestMapping(value = {"/expire-author"}, method = RequestMethod.POST)
    public @ResponseBody Long expireAuthor(@RequestParam Long id) throws ServiceException {
        authorAction.makeExpiredAuthor(id);
        return id;
    }

    @RequestMapping(value = {"/create-author"}, method = RequestMethod.POST)
    public @ResponseBody Author createAuthor(@Valid @ModelAttribute Author author,
                                             BindingResult validationResult) throws ServiceException {
        if (validationResult.hasErrors()){
            String errorMessage = validationResult.getAllErrors().get(0).getDefaultMessage();
            author.setName(errorMessage);
        } else {
            authorAction.createAuthor(author);
        }
        return author;
    }
}