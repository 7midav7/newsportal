package com.marchenko.news.controller;

import com.marchenko.news.entity.Comment;
import com.marchenko.news.exception.ServiceException;
import com.marchenko.news.service.interfaces.CommentAction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Map;

/**
 * Created by Marchenko Vadim on 4/26/2016.
 */

@Controller
public class CommentController {
    @Autowired
    private CommentAction commentAction;

    @RequestMapping(value = "/add-comment", method = RequestMethod.POST)
    public @ResponseBody Comment addComment(@Valid @ModelAttribute Comment comment, BindingResult result)
            throws ServiceException {
        if (result.hasErrors()){
            String errorMessage = result.getAllErrors().get(0).getDefaultMessage();
            comment.setText(errorMessage);
        } else {
            commentAction.create(comment);
        }
        return comment;
    }

    @RequestMapping(value = "/remove-comment", method = RequestMethod.POST)
    public @ResponseBody Long removeComment(@RequestParam Long id) throws ServiceException{
        commentAction.delete(id);
        return id;
    }
}
