package com.marchenko.news.logging;

import com.marchenko.news.exception.ControllerException;
import com.marchenko.news.exception.DaoException;
import com.marchenko.news.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * Created by Marchenko Vadim on 4/1/2016.
 */

@Aspect
@Component
public class TraceLogger {
    private enum Layer{DAO, SERVICE, CONTROLLER}
    private static final Logger LOGGER = LogManager.getLogger(TraceLogger.class);

    @Around("execution(* com.marchenko.news.controller.*.*(..))")
    public Object logAdminControllerInformation(ProceedingJoinPoint joinPoint) throws Throwable {
        try{
            logBeforeMessage(Layer.CONTROLLER, joinPoint);
            Object returnValue = joinPoint.proceed();
            logAfterMessage(Layer.CONTROLLER, joinPoint, returnValue);
            return returnValue;
        } catch (Throwable e){
            if (e instanceof ControllerException){
                throw e;
            } else {
                throw new ControllerException("Aspect catch exception in controller layer( "
                        + joinPoint.getTarget().getClass().getName() + " )", e);
            }
        }
    }

    @Around("execution(* com.marchenko.news.service.impl.*.*(..))")
    public Object logServiceInformation(ProceedingJoinPoint joinPoint) throws Throwable {
        try {
            logBeforeMessage(Layer.SERVICE, joinPoint);
            Object returnValue = joinPoint.proceed();
            logAfterMessage(Layer.SERVICE, joinPoint, returnValue);
            return returnValue;
        } catch (Throwable e){
            if (e instanceof ServiceException) {
                throw e;
            } else {
                throw new ServiceException("Aspect catch exception in service layer( "
                        + joinPoint.getTarget().getClass().getName() + " )",e);
            }
        }
    }

    @Around("execution(* com.marchenko.news.dao.impl.*.*(..))")
    public Object logDaoInformation(ProceedingJoinPoint joinPoint) throws Throwable {
        try{
            logBeforeMessage(Layer.DAO, joinPoint);
            Object returnValue = joinPoint.proceed();
            logAfterMessage(Layer.DAO, joinPoint, returnValue);
            return returnValue;
        } catch (Throwable e){
            if (e instanceof DaoException) {
                throw e;
            } else {
                throw new DaoException("Aspect catch exception in dao layer( "
                        + joinPoint.getTarget().getClass().getName() + " )", e);
            }
        }
    }

    private void addMethodInformation(StringBuilder message, ProceedingJoinPoint joinPoint){
        message.append(" at ").append(joinPoint.getTarget().getClass().getName());
        message.append(".").append(joinPoint.getSignature().getName()).append("(");
        Object[] objects = joinPoint.getArgs();
            for (Object obj : objects) {
                message.append(" ");
                if (obj != null) {
                    message.append(obj.toString());
                } else{
                    message.append("null");
                }
                message.append(",");
            }
        message.append(")");
    }

    private String logBeforeMessage(Layer type, ProceedingJoinPoint joinPoint){
        StringBuilder message = new StringBuilder(type.toString());
        message.append(":START ");
        addMethodInformation(message, joinPoint);
        LOGGER.trace(message);
        return message.toString();
    }

    private String logAfterMessage(Layer type, ProceedingJoinPoint joinPoint, Object returnValue){
        StringBuilder message = new StringBuilder(type.toString());
        message.append(":END ");
        addMethodInformation(message, joinPoint);
        message.append("return value - ");
        if (returnValue != null){
            message.append(returnValue.toString());
        } else {
            message.append("void");
        }
        LOGGER.trace(message);
        return message.toString();
    }
}
