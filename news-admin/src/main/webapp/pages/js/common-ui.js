var NAME_EDITING_LINK = "edit";
var NAME_UPDATING_LINK = "update";
var NAME_TAGS_CONTAINER = "list";
var NAME_SAVING_LINK = "save";
var NAME_BLOCK = "list-item";
var NAME_CANCELLATION_LINK = "cancel";
var NAME_INPUT = "item-input";
var NAME_ADDITIONAL_INPUT = "additional-item";
var PART_ID_FOR_UPDATING_ERROR = "updating-error";
var DIV_ID_FOR_SAVING_ERROR = "saving-error";
var CSRF_ID = "_csrf";
var CSRF_HEADER_ID = "_csrf_header";

function sendRequest(nameStatusInput, xmlHttpRequest, responseHandler, body){
    var addingInput = document.getElementById(nameStatusInput);
    addingInput.style.border = "2px solid yellow";

    xmlHttpRequest.onreadystatechange = function () {
        if (this.readyState != 4) return;
        if (this.status == 200) {
            addingInput.value = "";
            addingInput.style.border = "2px inset";
            responseHandler(xmlHttpRequest);
            return;
        }
        addingInput.style.border = "2px solid red";
    };
    xmlHttpRequest.send(body);
}

function addNamedLinkWithListener(linkName, id, nameListener) {
    var editionInput = document.getElementById(NAME_INPUT + id);
    var editionInputParent = editionInput.parentNode;
    var menuNode = document.createElement("span");
    var textNode = document.createTextNode(linkName);
    menuNode.appendChild(textNode);
    menuNode.setAttribute("href", "#");
    menuNode.setAttribute("onclick", nameListener + "(" + id + ")");
    menuNode.setAttribute("id", linkName + id);
    editionInputParent.appendChild(menuNode);
}

function preparationEditing(id) {
    document.getElementById(NAME_EDITING_LINK + id).style.display = "none";
    var input = document.getElementById(NAME_INPUT + id);
    input.removeAttribute("disabled");
    input.value = input.getAttribute("placeholder");
    input.style.border = "2px inset";
    var errorDiv = document.getElementById(PART_ID_FOR_UPDATING_ERROR + id);
    errorDiv.innerHTML = "";
}

function settingPostRequest(xmlHttpRequest){
    var token = document.getElementById(CSRF_ID).innerHTML;
    var header = document.getElementById(CSRF_HEADER_ID).innerHTML;
    xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlHttpRequest.setRequestHeader(header, token);
}