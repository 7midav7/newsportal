var NAME_DELETION_LINK = "delete";
var VALUE_FOR_MARK_INVALID_TAG_IN_UPDATING = 1000000000000000;

function editTagListener(id) {
    addNamedLinkWithListener(NAME_UPDATING_LINK, id, 'updatingTagListener');
    addNamedLinkWithListener(NAME_DELETION_LINK, id, 'deletionTagListener');
    addNamedLinkWithListener(NAME_CANCELLATION_LINK, id, 'cancellationTagEditingListener');
    preparationEditing(id);
    return false;
}

function savingTagListener() {
    var text = document.getElementById(NAME_ADDITIONAL_INPUT).value;
    var errorDiv = document.getElementById(DIV_ID_FOR_SAVING_ERROR);
    errorDiv.innerHTML = "";

    var xhr = new XMLHttpRequest();
    var body = "tagName=" + encodeURI(text);
    xhr.open("POST", "create-tag", true);
    settingPostRequest(xhr);
    sendRequest(NAME_ADDITIONAL_INPUT, xhr, savingTagResponseHandler, body);
}

function savingTagResponseHandler(xmlHttpRequest){
    var tag = JSON.parse(xmlHttpRequest.responseText);
    var id = tag.id;
    var text = tag.tagName;
    if (id != null){
        makeTagBlock(text, id);
    } else {
        var errorDiv = document.getElementById(DIV_ID_FOR_SAVING_ERROR);
        errorDiv.innerHTML = tag.tagName;
    }
}

function makeTagBlock(text, id) {
    var divNode = document.createElement("div");
    var inputNode = document.createElement("input");
    var errorDiv = document.createElement("div");
    inputNode.value = "";
    inputNode.id = NAME_INPUT + id;
    inputNode.disabled = "disabled";
    inputNode.placeholder = text;
    divNode.className = NAME_BLOCK;
    divNode.appendChild(inputNode);
    errorDiv.className = "error-text";
    errorDiv.id = PART_ID_FOR_UPDATING_ERROR + id;
    var containerNode = document.getElementById(NAME_TAGS_CONTAINER);
    containerNode.insertBefore(errorDiv, containerNode.firstElementChild)
    containerNode.insertBefore(divNode, errorDiv);
    addNamedLinkWithListener(NAME_EDITING_LINK, id, 'editTagListener');
}

function deletionTagListener(id) {
    var nameInputField = NAME_INPUT + id;
    var xmlHttpRequest = createDeletionRequest(id);
    var body = "id=" + encodeURI(id);
    sendRequest(nameInputField, xmlHttpRequest, deletionTagRequestHandler, body);
}

function createDeletionRequest(){
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "delete-tag", true);
    settingPostRequest(xhr);
    return xhr;
}

function deletionTagRequestHandler(xmlHttpRequest){
    var id = xmlHttpRequest.responseText;
    document.getElementById(NAME_INPUT + id).parentNode.remove();
}

function updatingTagListener(id) {
    var input = document.getElementById(NAME_INPUT + id);
    var text = input.value;
    var errorDiv = document.getElementById(PART_ID_FOR_UPDATING_ERROR + id);
    errorDiv.innerHTML = "";

    var xmlHttpRequest = createUpdatingTagRequest(text, id);
    var body = "tagName=" + encodeURIComponent(text) + "&id=" + encodeURI(id);
    sendRequest(NAME_INPUT + id, xmlHttpRequest, updatingRequestHandler, body);

    hideMenuLinksForTag(id);
}

function createUpdatingTagRequest(text, id){
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "update-tag", true);
    settingPostRequest(xhr);
    return xhr;
}

function updatingRequestHandler(xmlHttpRequest) {
    var tag = JSON.parse(xmlHttpRequest.responseText);
    if (tag.id < VALUE_FOR_MARK_INVALID_TAG_IN_UPDATING) {
        var inputNode = document.getElementById(NAME_INPUT + tag.id);
        inputNode.setAttribute("placeholder", tag.tagName);
    } else {
        var errorDiv = document.getElementById(PART_ID_FOR_UPDATING_ERROR +
            (tag.id - VALUE_FOR_MARK_INVALID_TAG_IN_UPDATING) );
        errorDiv.innerHTML = tag.tagName;
    }
}

function cancellationTagEditingListener(id) {
    hideMenuLinksForTag(id);
}

function hideMenuLinksForTag(id) {
    document.getElementById(NAME_CANCELLATION_LINK + id).remove();
    document.getElementById(NAME_UPDATING_LINK + id).remove();
    document.getElementById(NAME_DELETION_LINK + id).remove();

    document.getElementById(NAME_EDITING_LINK + id).style.display = "initial";
    document.getElementById(NAME_INPUT + id).setAttribute("disabled", 'disabled');
    document.getElementById(NAME_INPUT + id).value = "";
}