var NAME_COMMENT_DIV = "comment";

function postComment(id){
    var commentDiv = document.createElement("div");
    var commentDateWrapperDiv = document.createElement("div");
    var commentDateDiv = document.createElement("div");
    var commentText = document.getElementsByClassName("writing-comment-field").item(0).value;
    var writingCommentField = document.getElementsByClassName("writing-comment-field").item(0);
    var commentTextNode = document.createTextNode(commentText);
    var pendingTextNode = document.createTextNode("pending...");

    commentDiv.className = "comment";
    commentDateWrapperDiv.className = "comment-date-wrapper clearfix";
    commentDateDiv.className = "comment-date";

    document.getElementsByClassName("comments-section").item(0).insertBefore(commentDiv, writingCommentField);
    commentDiv.appendChild(commentDateWrapperDiv);
    commentDiv.appendChild(commentTextNode);
    commentDateWrapperDiv.appendChild(commentDateDiv);
    commentDateDiv.appendChild(pendingTextNode);

    commentDateDiv.style.color = "#dd0";
    writingCommentField.value = "";
    sendRequestPostingComment(id, commentText, commentDateDiv);
}

function sendRequestPostingComment(id, text, statusNode){
    var xhr = new XMLHttpRequest();
    var body = "newsId=" + encodeURI(id) + "&text=" + encodeURI(text);
    xhr.open("POST","add-comment", true);
    settingPostRequest(xhr);
    xhr.onreadystatechange = function () {
        if (this.readyState != 4) return;
        if (this.status == 200) {
            var status = parseStatusFromResponse(xhr);
            statusNode.innerHTML = status;
            statusNode.style.color = "#000";
        }
    };
    xhr.send(body);
}

function parseStatusFromResponse(xmlHttpRequest){
    var comment = JSON.parse(xmlHttpRequest.responseText);
    if (comment.id != null){
        var date = comment.creationDate;
        var month = date.monthValue;
        var day = date.dayOfMonth;
        if (month.toString().length == 1){
            month = "0" + month;
        }
        if (day.toString().length == 1){
            day = "0" + day;
        }
        return month + "/" + day + "/" + date.year;
    } else {
        return comment.text;
    }
}

function deleteComment(id){
    var deletionLink = document.getElementById(NAME_DELETION_LINK + id);
    deletionLink.style.color = "yellow";
    deletionLink.innerHTML = "pending...";
    sendRequestDeletionComment(id, deletionLink);
    return false;
}

function sendRequestDeletionComment(id, deletionLink){
    var xhr = new XMLHttpRequest();
    var body = "id=" + encodeURI(id);
    xhr.open("POST","remove-comment", true);
    settingPostRequest(xhr);
    xhr.onreadystatechange = function(){
        if (this.readyState != 4) return;
        if (this.status == 200){
            var deletionBlock = document.getElementById(NAME_COMMENT_DIV + id);
            deletionBlock.remove();
        }
    };
    xhr.send(body);
}
