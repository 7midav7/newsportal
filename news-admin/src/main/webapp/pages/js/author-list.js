var NAME_EXPIRE_LINK = "expire";

function editAuthorListener(id) {
    addNamedLinkWithListener(NAME_UPDATING_LINK, id, 'updatingAuthorListener');
    addNamedLinkWithListener(NAME_EXPIRE_LINK, id, 'expirationAuthorListener');
    addNamedLinkWithListener(NAME_CANCELLATION_LINK, id, 'cancellationAuthorEditingListener');
    preparationEditing(id);
    return false;
}

function savingAuthorListener() {
    var name = document.getElementById(NAME_ADDITIONAL_INPUT).value;
    var errorDiv = document.getElementById(DIV_ID_FOR_SAVING_ERROR);
    errorDiv.innerHTML = "";

    var xhr = new XMLHttpRequest();
    var body = "name=" + encodeURI(name);
    xhr.open("POST", "create-author", true);
    settingPostRequest(xhr);

    sendRequest(NAME_ADDITIONAL_INPUT, xhr, savingAuthorResponseHandler, body);
}

function savingAuthorResponseHandler(xmlHttpRequest){
    var author = JSON.parse(xmlHttpRequest.responseText);
    var id = author.id;
    var name = author.name;
    if (id != null) {
        makeAuthorBlock(name, id);
    } else {
        var errorDiv = document.getElementById(DIV_ID_FOR_SAVING_ERROR);
        errorDiv.innerHTML = author.name;
    }
}

function makeAuthorBlock(name, id) {
    var divNode = document.createElement("div");
    var inputNode = document.createElement("input");
    var errorDiv = document.createElement("div");
    inputNode.value = "";
    inputNode.id = NAME_INPUT + id;
    inputNode.disabled = "disabled";
    inputNode.placeholder = name;
    divNode.className = NAME_BLOCK;
    divNode.appendChild(inputNode);
    errorDiv.className = "error-text";
    errorDiv.id = PART_ID_FOR_UPDATING_ERROR + id;
    var containerNode = document.getElementById(NAME_TAGS_CONTAINER);
    containerNode.insertBefore(errorDiv, containerNode.firstElementChild);
    containerNode.insertBefore(divNode, errorDiv);
    addNamedLinkWithListener(NAME_EDITING_LINK, id, 'editAuthorListener');
}

function expirationAuthorListener(id) {
    var nameInputField = NAME_INPUT + id;
    var xmlHttpRequest = createExpirationRequest(id);
    var body = "id=" + encodeURI(id);
    sendRequest(nameInputField, xmlHttpRequest, expirationAuthorRequestHandler, body);
}

function createExpirationRequest(){
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "expire-author", true);
    settingPostRequest(xhr);
    return xhr;
}

function expirationAuthorRequestHandler(xmlHttpRequest){
    var id = xmlHttpRequest.responseText;
    document.getElementById(NAME_INPUT + id).parentNode.remove();
}

function updatingAuthorListener(id) {
    var input = document.getElementById(NAME_INPUT + id);
    var text = input.value;
    var xmlHttpRequest = createUpdatingAuthorRequest(text, id);
    var body = "name=" + encodeURIComponent(text) + "&id=" + encodeURI(id)
    sendRequest(NAME_INPUT + id, xmlHttpRequest, updatingAuthorRequestHandler, body);
    hideMenuLinksForAuthor(id);
}

function createUpdatingAuthorRequest(){
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "update-author", true);
    settingPostRequest(xhr);
    return xhr;
}

function updatingAuthorRequestHandler(xmlHttpRequest) {
    var author = JSON.parse(xmlHttpRequest.responseText);
    if (author.id < VALUE_FOR_MARK_INVALID_TAG_IN_UPDATING) {
        var inputNode = document.getElementById(NAME_INPUT + author.id);
        inputNode.setAttribute("placeholder", author.name);
    } else {
        var errorDiv = document.getElementById( PART_ID_FOR_UPDATING_ERROR + (author.id - VALUE_FOR_MARK_INVALID_TAG_IN_UPDATING) );
        errorDiv.innerHTML = author.name;
    }
}

function cancellationAuthorEditingListener(id) {
    hideMenuLinksForAuthor(id);
}

function hideMenuLinksForAuthor(id){
    document.getElementById(NAME_CANCELLATION_LINK + id).remove();
    document.getElementById(NAME_UPDATING_LINK + id).remove();
    document.getElementById(NAME_EXPIRE_LINK + id).remove();

    document.getElementById(NAME_EDITING_LINK + id).style.display = "initial";
    document.getElementById(NAME_INPUT + id).setAttribute("disabled", 'disabled');
    document.getElementById(NAME_INPUT + id).value = "";
}