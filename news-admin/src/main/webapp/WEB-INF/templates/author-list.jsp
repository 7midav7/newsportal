<%@page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="list-wrapper">
    <div class="list-item">
        <input type="text" id="additional-item" maxlength="30" placeholder="Add author ...">
        <span id="save" onclick="savingAuthorListener(${tag.id})">save</span>
        <div class="error-text" id="saving-error"></div>
    </div>
    <div id="list">
        <c:forEach var="author" items="${authors}">
            <div class="list-item">
                <input type="text" id="item-input${author.id}" placeholder="${author.name}"
                       disabled maxlength="30">
                <span id="edit${author.id}" onclick="editAuthorListener(${author.id})">edit</span>
            </div>
            <div class="error-text" id="updating-error${author.id}"></div>
        </c:forEach>
    </div>
</div>