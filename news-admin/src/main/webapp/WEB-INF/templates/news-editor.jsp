<%@page pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="news-content clearfix">
    <form:form method="post" action="edit-news" modelAttribute="news">
        <form:hidden path="id"/>
        <div class="edit-block">
            <label for="news-title">Title:</label>
            <form:input path="title" id="news-title" maxlength="30"/>
            <div>
                <form:errors path="title" id="news-title" cssClass="error-text" maxlength="30"/>
            </div>
            <fmt:parseDate value="${news.modificationDate}" var="parsedDateFromString" pattern="yyyy-MM-dd"
                           type="date"/>
            <fmt:formatDate value="${parsedDateFromString}" var="parsedDate" pattern="MM/dd/yyyy" type="date"/>
            <span class="news-date"><c:out value="${parsedDate}"/></span>
        </div>
        <div class="edit-block">
            <label for="author-select">Author:</label>
            <select id="author-select" name="authorId">
                <c:forEach var="author" items="${authorList}">
                    <option
                            <c:if test="${author == newsAuthor}">selected</c:if>
                            value="${author.id}">${author.name}</option>
                </c:forEach>
            </select>
        </div>
        <div class="edit-block">
            <label for="brief-text">Brief:</label>
            <form:textarea path="shortText" id="brief-text" class="brief-text"/>
            <form:errors path="shortText" cssClass="error-text"/>
        </div>
        <div class="edit-block">
            <label for="full-text">Text:</label>
            <form:textarea path="fullText" id="full-text" class="full-text"/>
            <form:errors path="fullText" cssClass="error-text"/>
        </div>
        <div class="edit-block clearfix">
            <label for="tags">Tags:</label>
            <select id="tags" name="tagsId" multiple>
                <c:forEach var="actualTag" items="${actualTags}">
                    <option value="${actualTag.id}" selected>${actualTag.tagName}</option>
                </c:forEach>
                <c:forEach var="otherTag" items="${otherTags}">
                    <option value="${otherTag.id}">${otherTag.tagName}</option>
                </c:forEach>
            </select>
            <input type="submit" class="submit-btn" value="Edit">
        </div>
    </form:form>
    <hr>
    <div class="comments-section">
        <c:forEach var="comment" items="${comments}">
            <div class="comment" id="comment${comment.id}">
                <div class="comment-date-wrapper clearfix">
                    <div class="comment-date">
                        <fmt:parseDate value="${comment.creationDate}" var="parsedDateFromString" pattern="yyyy-MM-dd"
                                       type="date"/>
                        <fmt:formatDate value="${parsedDateFromString}" var="parsedDate" pattern="MM/dd/yyyy"
                                        type="date"/>
                        <c:out value="${parsedDate}"/>
                    </div>
                </div>
                <div class="clearfix">
                    <span><c:out value="${comment.text}"/></span>
                    <span class="delete-link" onclick="deleteComment(${comment.id})"
                          id="delete${comment.id}">delete</span>
                </div>
            </div>
        </c:forEach>
    </div>
</div>
