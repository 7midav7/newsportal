<%@page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="list-wrapper">
    <div class="list-item">
        <input type="text" id="additional-item" placeholder="Add tag ..." maxlength="30">
        <span href="#" id="save" onclick="savingTagListener(${tag.id})">save</span>
        <div id="saving-error" class="error-text"></div>
    </div>
    <div id="list">
        <c:forEach var="tag" items="${tags}">
            <div class="list-item">
                <input type="text" id="item-input${tag.id}" placeholder="${tag.tagName}"
                       disabled maxlength="30">
                <span id="edit${tag.id}" onclick="editTagListener(${tag.id})">edit</span>
            </div>
            <div id="updating-error${tag.id}" class="error-text"></div>
        </c:forEach>
    </div>
</div>