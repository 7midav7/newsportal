<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="clearfix">
    <span id="header">News portal - Administration</span>
    <a id="logout-link" href="#"
       onclick="document.getElementById('logoutForm').submit()"> Logout</a>

    <form action="${pageContext.request.contextPath}/logout" method="POST" id="logoutForm">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>
</div>
