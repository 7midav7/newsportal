<%@page pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<div class="nav">
    <div>
        <a class="nav-item" href="<spring:url value='/newses'/>"   >News List</a>
    </div>
    <div>
        <a class="nav-item" href="<spring:url value='/news-creator'/>" >Add News</a>
    </div>
    <div>
        <a class="nav-item" href="<spring:url value='/authors'/>"  >Add/Update Authors</a>
    </div>
    <div>
        <a class="nav-item" href="<spring:url value='/tags'/>"     >Add/Update Tags</a>
    </div>
</div>