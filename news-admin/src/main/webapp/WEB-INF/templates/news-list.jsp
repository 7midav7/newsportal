<%@page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="news-list-wrapper">
    <div class="filter-wrapper clearfix">
        <form id="filterForm"  action="filter-on">
            <span>Author:</span>
            <select id="author-select" name="authorId">
                <option value="">Choose ...</option>
                <c:forEach var="author" items="${authors}">
                    <option value="${author.id}" <c:if test="${author.id == filterAuthor}">selected</c:if>
                    >${author.name}</option>
                </c:forEach>
            </select>
            <span>Tags:</span>
            <select id="tags-select" name="tagIds"  multiple>
                <c:forEach var="tag" items="${filterTags}">
                    <option value="${tag.id}" selected>${tag.tagName}</option>
                </c:forEach>
                <c:forEach var="tag" items="${otherTags}">
                    <option value="${tag.id}">${tag.tagName}</option>
                </c:forEach>
            </select>
        </form>
        <div class="clearfix">
            <button id="resetButton" onclick="window.open('<spring:url value="/filter-off"/>','_self')">Reset</button>
            <button id="filterButton" type="submit" form="filterForm">Filter</button>
        </div>
    </div>
    <hr>
    <form id="deletionForm" action="delete-newses">
        <c:forEach var="newsBlock" items="${newses}">
            <div class="news-wrapper clearfix">
                <a class="news-title" href="<spring:url value="/view-news"/>?id=<c:out value="${newsBlock.news.id}"/>">
                    <c:out value="${newsBlock.news.title}"/>
                </a>
                <span class="news-author">( by <c:out value="${newsBlock.author.name}"/> )</span>
                <fmt:parseDate value="${newsBlock.news.modificationDate}" var="parsedDateFromString" pattern="yyyy-MM-dd" type="date"/>
                <fmt:formatDate value="${parsedDateFromString}" var="parsedDate" pattern="MM/dd/yyyy" type="date"/>
                <span class="news-date"><c:out value="${parsedDate}"/></span>
                <p class="news-short-text"><c:out value="${newsBlock.news.shortText}"/></p>
                <c:forEach var="tag" items="${newsBlock.tags}">
                    <span class="news-tag"><c:out value="${tag.tagName}"/></span>
                </c:forEach>
                <input type="checkbox" name="newsIdList" value="${newsBlock.news.id}">
                <a class="edit-link"
                   href="<spring:url value="/news-editor"/>?id=<c:out value="${newsBlock.news.id}"/>">Edit</a>
                <span class="comment-text">Comments(${fn:length(newsBlock.comments)})</span>
            </div>
        </c:forEach>
    </form>
    <div class="pagination">
        <c:if test="${currentPage != 1}">
            <button onclick="window.open('<spring:url value="/newses?page=1${filterUri}"/>','_self')">1</button>
        </c:if>
        <c:if test="${( currentPage - 2 > 1) && (currentPage - 2 < pageCount ) }">
            <button onclick="window.open('<spring:url
                    value="/newses?page=${currentPage - 2}${filterUri}"/>','_self')">${currentPage - 2}</button>
        </c:if>
        <c:if test="${( currentPage - 1 > 1) && (currentPage - 1 < pageCount ) }">
            <button onclick="window.open('<spring:url
                    value="/newses?page=${currentPage - 1}${filterUri}"/>','_self')">${currentPage - 1}</button>
        </c:if>
        <button disabled>${currentPage}</button>
        <c:if test="${( currentPage + 1 > 1) && (currentPage + 1 < pageCount ) }">
            <button onclick="window.open('<spring:url
                    value="/newses?page=${currentPage + 1}${filterUri}"/>','_self')">${currentPage + 1}</button>
        </c:if>
        <c:if test="${( currentPage + 2 > 1) && (currentPage + 2 < pageCount) }">
            <button onclick="window.open('<spring:url
                    value="/newses?page=${currentPage + 2}${filterUri}"/>','_self')">${currentPage + 2}</button>
        </c:if>
        <c:if test="${currentPage != pageCount}">
            <button onclick="window.open('<spring:url value="/newses?page=${pageCount}${filterUri}"/>','_self')">${pageCount}</button>
        </c:if>
    </div>
    <div class="clearfix">
        <button id="deleteButton" form="deletionForm">Delete</button>
    </div>
</div>