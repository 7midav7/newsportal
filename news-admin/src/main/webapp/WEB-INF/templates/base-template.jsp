<%@page pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<html>
<link rel="stylesheet" href="<c:url value='/pages/css/news.css'/>">
<link rel="stylesheet" href="<c:url value="/pages/css/base-template.css"/>">
<link rel="stylesheet" href="<c:url value="/pages/css/news-list.css"/>">
<link rel="stylesheet" href="<c:url value="/pages/css/navigation.css"/>">
<link rel="stylesheet" href="<c:url value="/pages/css/news-creator.css"/>">
<script src="<c:url value='/pages/js/common-ui.js'/>"></script>
<script src="<c:url value='/pages/js/tag-list.js'/>"></script>
<script src="<c:url value='/pages/js/author-list.js'/>"></script>
<script src="<c:url value='/pages/js/news.js'/>"></script>
</html>
<body>
<div hidden id="_csrf_header">${_csrf.headerName}</div>
<div hidden id="_csrf">${_csrf.token}</div>
  <header>
      <tiles:insertAttribute name="header"/>
      <hr>
  </header>
  <div class="clearfix">
      <nav>
          <tiles:insertAttribute name="nav"/>
      </nav>
      <main>
          <tiles:insertAttribute name="content"/>
      </main>
  </div>
  <footer>
      <hr>
      <tiles:insertAttribute name="footer"/>
  </footer>
</body>