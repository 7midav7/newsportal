<%@page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<div class="news-content clearfix">
    <form:form method="post" action="create-news" modelAttribute="news">
        <form:hidden path="id"/>
        <div class="edit-block">
            <label for="news-title">Title:</label>
            <form:input path="title" id="news-title" maxlength="30"/>
            <div>
                <form:errors path="title" cssClass="error-text"/>
            </div>
        </div>
        <div class="edit-block">
            <label for="author-select">Author:</label>
            <select id="author-select" name="authorId">
                <c:forEach var="author" items="${authorList}">
                    <option value="${author.id}">${author.name}</option>
                </c:forEach>
            </select>
        </div>
        <div class="edit-block">
            <label for="brief-text">Brief:</label>
            <form:textarea path="shortText" id="brief-text" cssClass="brief-text" maxlength="100"/>
            <form:errors path="shortText" cssClass="error-text"/>
        </div>
        <div class="edit-block">
            <label for="full-text">Text:</label>
            <form:textarea path="fullText" id="full-text" class="full-text" maxlength="1000"/>
            <form:errors path="fullText" cssClass="error-text"/>
        </div>
        <div class="edit-block">
            <label for="tags">Tags:</label>
            <select id="tags" name="tagsId" multiple>
                <c:forEach var="tag" items="${tags}">
                    <option value="${tag.id}">${tag.tagName}</option>
                </c:forEach>
            </select>
        </div>
        <input type="submit" class="submit-btn" value="Submit">
    </form:form>
</div>