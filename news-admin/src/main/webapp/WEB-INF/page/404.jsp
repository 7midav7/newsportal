<%@page pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<html>
<link rel="stylesheet" href="<c:url value='/pages/css/news.css'/>">
<link rel="stylesheet" href="<c:url value="/pages/css/base-template.css"/>">
<link rel="stylesheet" href="<c:url value="/pages/css/news-list.css"/>">
<link rel="stylesheet" href="<c:url value="/pages/css/navigation.css"/>">
<link rel="stylesheet" href="<c:url value="/pages/css/news-creator.css"/>">
<script src="<c:url value='/pages/js/common-ui.js'/>"></script>
<script src="<c:url value='/pages/js/tag-list.js'/>"></script>
<script src="<c:url value='/pages/js/author-list.js'/>"></script>
<script src="<c:url value='/pages/js/news.js'/>"></script>
</html>
<body>
<header>
    <c:import url="/WEB-INF/templates/header.jsp"/>
    <hr>
</header>
<div class="clearfix">
    <nav>
        <c:import url="/WEB-INF/templates/navigation.jsp"/>
    </nav>
    <main>
        <div class="error-message">SORRY. PAGE NOT FOUND</div>
    </main>
</div>
<footer>
    <hr>
    <c:import url="/WEB-INF/templates/footer.jsp"/>
</footer>
</body>