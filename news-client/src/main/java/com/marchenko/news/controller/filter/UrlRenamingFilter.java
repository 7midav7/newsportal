package com.marchenko.news.controller.filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Marchenko Vadim on 5/10/2016.
 */

@WebFilter(filterName = "UrlRewriteFilter", urlPatterns = {"/*"})
public class UrlRenamingFilter implements Filter {
    private static final String[] ALLOWED_EXTENSIONS = new String[]{ ".css",
            ".js"};
    private static final String ALLOWED_SERVLET = "/controller";
    private static final String RIGHT_URL_BEGINNING = "/controller?command=";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;;
        String contextPath = request.getServletPath();
        if (isContextPathAllowed(contextPath)){
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            if (isContextPathNeededToRename(contextPath)){
                renameUrlAndForwardRequest(contextPath, request, response);
            }
        }
    }

    @Override
    public void destroy() {

    }

    private boolean isContextPathAllowed(String contextPath){
        for (String allowedExtension : ALLOWED_EXTENSIONS){
            if (contextPath.contains(allowedExtension)){
                return true;
            }
        }
        return contextPath.contains(ALLOWED_SERVLET);
    }

    private boolean isContextPathNeededToRename(String contextPath){
        return !contextPath.contains(".");
    }

    private void renameUrlAndForwardRequest(String contextPath, HttpServletRequest request,
                                            HttpServletResponse response) throws ServletException, IOException {
        String[] parts = contextPath.split("\\?");
        StringBuilder newUrl = new StringBuilder(RIGHT_URL_BEGINNING);
        newUrl.append(parts[0].substring(1));
        RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(newUrl.toString());
        dispatcher.forward(request, response);
    }
}
