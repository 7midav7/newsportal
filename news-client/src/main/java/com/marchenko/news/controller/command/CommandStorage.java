package com.marchenko.news.controller.command;

import java.util.Map;

/**
 * Created by Marchenko Vadim on 5/9/2016.
 */

public class CommandStorage {
    private final Map<String, Command> commandMap;

    CommandStorage(Map<String, Command> map){
        commandMap = map;
    }

    public Command takeCommand(String name){
        return commandMap.get(name);
    }
}
