package com.marchenko.news.controller.command;

import com.marchenko.news.controller.ControllerException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Marchenko Vadim on 5/9/2016.
 */
public interface Command {
    String execute(HttpServletRequest request,
                   HttpServletResponse response) throws ControllerException;

}
