package com.marchenko.news.controller.command;

import com.marchenko.news.controller.ControllerException;
import com.marchenko.news.controller.util.SessionAction;
import com.marchenko.news.entity.SearchCriteria;
import com.marchenko.news.entity.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Marchenko Vadim on 5/10/2016.
 */

@Component("filterOnCommand")
public class FilterOnCommand implements Command {
    private static final String KEY_SELECTED_AUTHOR_IN_FILTER = "authorId";
    private static final String KEY_SELECTED_TAGS_IN_FILTER = "tagIds";
    private static final String REDIRECT_NEWS_LIST_PAGE = "redirect:/newses";
    @Autowired
    private SessionAction sessionAction;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ControllerException {
        SearchCriteria searchCriteria = new SearchCriteria();
        List<Long> tagIds = new ArrayList<>();
        Long authorId =  takeAuthorTagIdsFromRequest(request, tagIds);

        if (authorId != null) {
            searchCriteria.setAuthorId(authorId);
        }
        if (tagIds != null) {
            searchCriteria.setTagIdList(tagIds);
        }

        sessionAction.putSearchCriteria(request, searchCriteria);

        return REDIRECT_NEWS_LIST_PAGE;
    }

    private Long takeAuthorTagIdsFromRequest(HttpServletRequest request,
                                            List<Long> result){
        String stringAuthorId = request.getParameter(KEY_SELECTED_AUTHOR_IN_FILTER);
        Long authorId;
        if (stringAuthorId != null && !"".equals(stringAuthorId)) {
             authorId = Long.valueOf(stringAuthorId);
        } else {
            authorId = null;
        }
        String[] stringTagIds = request.getParameterValues(KEY_SELECTED_TAGS_IN_FILTER);
        if (stringTagIds != null) {
            for (String stringTagId : stringTagIds) {
                if (stringTagId != null && !"".equals(stringTagId)) {
                    result.add(Long.valueOf(stringTagId));
                }
            }
        }
        return authorId;
    }
}
