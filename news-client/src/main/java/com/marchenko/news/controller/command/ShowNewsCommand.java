package com.marchenko.news.controller.command;

import com.marchenko.news.controller.ControllerException;
import com.marchenko.news.entity.Comment;
import com.marchenko.news.entity.NewsPost;
import com.marchenko.news.exception.ServiceException;
import com.marchenko.news.service.interfaces.NewsPostAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 5/10/2016.
 */

@Component("showNewsCommand")
public class ShowNewsCommand implements Command{
    private static final String PAGE_NEWS_VIEW = "/pages/jsp/news-view.jsp";
    private static final String KEY_NEWS_ID = "id";
    private static final String KEY_NEWS_POST = "post";
    @Autowired
    private NewsPostAction newsPostAction;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ControllerException {
        Long newsId = takeNewsId(request);
        try {
            Optional<NewsPost> optional = newsPostAction.readNews(newsId);
            if (optional.isPresent()){
                NewsPost newsPost = optional.get();

                List<Comment> commentList = newsPost.getComments();
                commentList.sort((a, b) -> a.getCreationDate().compareTo(b.getCreationDate()));

                request.setAttribute(KEY_NEWS_POST, newsPost);
            }
        } catch (ServiceException e) {
            throw new ControllerException("Exception in service layer newsId=" + newsId,
                    e);
        }
        return PAGE_NEWS_VIEW;
    }

    private Long takeNewsId(HttpServletRequest request){
        String strId = request.getParameter(KEY_NEWS_ID);
        if (strId != null && !"".equals(strId)){
            return Long.valueOf(strId);
        }
        return null;
    }
}
