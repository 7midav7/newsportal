package com.marchenko.news.controller.util;

import com.marchenko.news.controller.ControllerException;
import com.marchenko.news.entity.SearchCriteria;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Marchenko Vadim on 5/9/2016.
 */

@Component
public class SessionAction {
    private static final String SEARCH_CRITERIA_ATTRIBUTE = "searchCriteria";

    public SearchCriteria takeSearchCriteria(HttpServletRequest request){
        HttpSession session = request.getSession();
        SearchCriteria searchCriteria =
                (SearchCriteria) session.getAttribute(SEARCH_CRITERIA_ATTRIBUTE);
        return searchCriteria;
    }

    public void putSearchCriteria(HttpServletRequest request, SearchCriteria searchCriteria) throws ControllerException {
        HttpSession session = request.getSession();
        session.setAttribute(SEARCH_CRITERIA_ATTRIBUTE, searchCriteria);
    }
}
