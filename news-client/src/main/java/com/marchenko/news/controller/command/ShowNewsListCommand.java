package com.marchenko.news.controller.command;

import com.marchenko.news.controller.ControllerException;
import com.marchenko.news.controller.util.SessionAction;
import com.marchenko.news.entity.*;
import com.marchenko.news.exception.ServiceException;
import com.marchenko.news.service.interfaces.AuthorAction;
import com.marchenko.news.service.interfaces.NewsAction;
import com.marchenko.news.service.interfaces.NewsPostAction;
import com.marchenko.news.service.interfaces.TagAction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Marchenko Vadim on 5/9/2016.
 */

@Component("showNewsListCommand")
public class ShowNewsListCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final String PARAMETER_CURRENT_PAGE = "page";
    private static final String KEY_NEWS_POST_LIST = "newses";
    private static final String KEY_NUMBER_CURRENT_PAGE = "currentPage";
    private static final String KEY_PAGE_COUNT = "pageCount";
    private static final String KEY_SELECTED_AUTHOR_IN_FILTER = "filterAuthor";
    private static final String KEY_SELECTED_TAGS_IN_FILTER = "filterTags";
    private static final String KEY_NON_SELECTED_TAGS_IN_FILTER = "otherTags";
    private static final String KEY_AUTHOR_LIST = "authors";
    private static final String PAGE_NEWS_LIST = "/pages/jsp/news-list.jsp";
    private static final long NUMBER_NEWS_ON_PAGE = 3;
    @Autowired
    private AuthorAction authorAction;
    @Autowired
    private NewsAction newsAction;
    @Autowired
    private NewsPostAction newsPostAction;
    @Autowired
    private TagAction tagAction;
    @Autowired
    private SessionAction sessionAction;


    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws ControllerException {
        Long currentPage = takeCurrentPageIndexAttribute(request);
        Long firstNewsIndex = NUMBER_NEWS_ON_PAGE * (currentPage - 1) + 1;
        Long lastNewsIndex = NUMBER_NEWS_ON_PAGE * currentPage;

        putFilterAttributesInRequest(request);
        putPageContentInModel(request, firstNewsIndex, lastNewsIndex);
        request.setAttribute(KEY_NUMBER_CURRENT_PAGE, currentPage);

        return PAGE_NEWS_LIST;
    }

    private Long takeCurrentPageIndexAttribute(HttpServletRequest request){
        String idAttribute = request.getParameter(PARAMETER_CURRENT_PAGE);
        if (idAttribute != null ){
            Scanner scanner = new Scanner(idAttribute);
            if (scanner.hasNextLong()){
                return scanner.nextLong();
            }
        }
        return 1L;
    }

    private void putFilterAttributesInRequest(HttpServletRequest request) throws ControllerException {
        List<Tag> searchingTags = new ArrayList<>();
        SearchCriteria searchCriteria = sessionAction.takeSearchCriteria(request);
        List<Author> allAuthors;
        List<Tag> nonSearchingTags;
        Long authorId;

        try {
            allAuthors = authorAction.findNonExpiredAuthors();
            nonSearchingTags = tagAction.findAll();
            authorId = takeAuthorAndTagsFromCriteria(searchCriteria, searchingTags);
        } catch (ServiceException e){
            throw new ControllerException("Exception in service", e);
        }
        nonSearchingTags.removeAll(searchingTags);

        searchingTags.sort((a, b) -> a.getTagName().compareTo(b.getTagName()));
        nonSearchingTags.sort((a, b) -> a.getTagName().compareTo(b.getTagName()));

        request.setAttribute(KEY_AUTHOR_LIST, allAuthors);
        request.setAttribute(KEY_SELECTED_AUTHOR_IN_FILTER, authorId);
        request.setAttribute(KEY_SELECTED_TAGS_IN_FILTER, searchingTags);
        request.setAttribute(KEY_NON_SELECTED_TAGS_IN_FILTER, nonSearchingTags);
    }

    private void putPageContentInModel(HttpServletRequest request,
                                       Long firstIndex,
                                       Long lastIndex) throws ControllerException {
        List<News> newses;
        Long pageCount;
        List<NewsPost> posts;
        SearchCriteria searchCriteria = sessionAction.takeSearchCriteria(request);
        try {
            if (searchCriteria != null) {
                newses = newsAction.findBySearchCriteria(searchCriteria, firstIndex, lastIndex);
                pageCount = newsAction.countNewsesBySearchCriteria(searchCriteria);
            } else {
                newses = newsAction.takeMostCommentedNews(firstIndex, lastIndex);
                pageCount = newsAction.countNewses();
            }
            posts = newsPostAction.convert(newses);
        } catch (ServiceException e){
            throw new ControllerException("Exception in service firstIndex="
                    + firstIndex + " lastIndex=" + lastIndex + " with searchCriteria=" +
                    searchCriteria, e);
        }

        request.setAttribute(KEY_NEWS_POST_LIST, posts);
        pageCount = (pageCount + NUMBER_NEWS_ON_PAGE - 1) / NUMBER_NEWS_ON_PAGE;
        request.setAttribute(KEY_PAGE_COUNT, pageCount);
    }

    private Long takeAuthorAndTagsFromCriteria(SearchCriteria searchCriteria, List<Tag> resultTags) throws ServiceException {
        if (searchCriteria != null) {
            List<Long> tagIdList = searchCriteria.getTagIdList();
            if (tagIdList != null) {
                resultTags.addAll(tagAction.readSequence(tagIdList));
            }
            return searchCriteria.getAuthorId();
        } else {
            return null;
        }
    }

}
