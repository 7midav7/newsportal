package com.marchenko.news.controller.command;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.marchenko.news.controller.ControllerException;
import com.marchenko.news.entity.Comment;
import com.marchenko.news.exception.ServiceException;
import com.marchenko.news.service.interfaces.CommentAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Marchenko Vadim on 5/10/2016.
 */

@Component("postCommentCommand")
public class PostCommentCommand implements Command {
    private static final String KEY_NEWS_ID = "newsId";
    private static final String KEY_COMMENT_TEXT = "text";
    private static final int SIZE_RESTRICTION_ON_LENGTH_COMMENT = 100;
    private static final String INVALID_COMMENT_MESSAGE = "String is empty or" +
            " its size is more then " + SIZE_RESTRICTION_ON_LENGTH_COMMENT;
    @Autowired
    private CommentAction commentAction;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws ControllerException {
        StringBuilder commentText = new StringBuilder();
        Long newsId = takeNewsIdAndText(commentText, request);

        Comment comment = new Comment(commentText.toString());
        comment.setNewsId(newsId);

        try {
            if (comment.getNewsId() != null) {
                commentAction.create(comment);
            }
        } catch (ServiceException e) {
            throw new ControllerException("Exception in create function with comment="
            + comment, e);
        }

        ObjectMapper mapper = new ObjectMapper();
        String json;
        try {
            json = mapper.writeValueAsString(comment);
        } catch (JsonProcessingException e) {
            throw new ControllerException("Exception in creating json string" +
                    " with comment=" + comment, e);
        }
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        try {
            response.getWriter().write(json);
        } catch (IOException e) {
            throw new ControllerException("Exception in writing json to response" +
                    " json=" + json,e);
        }
        return null;
    }

    private Long takeNewsIdAndText(StringBuilder content, HttpServletRequest request){
        String text = request.getParameter(KEY_COMMENT_TEXT);
        Long newsId = null;
        if (text == null || text.isEmpty() || text.length() > SIZE_RESTRICTION_ON_LENGTH_COMMENT){
            text = INVALID_COMMENT_MESSAGE;
        } else {
            String strId = request.getParameter(KEY_NEWS_ID);
            if (strId != null && !"".equals(strId)) {
                newsId = Long.valueOf(strId);
            }
        }
        content.append(text);
        return newsId;
    }
}
