package com.marchenko.news.controller.command;

import com.marchenko.news.controller.ControllerException;
import com.marchenko.news.controller.util.SessionAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Marchenko Vadim on 5/10/2016.
 */

@Component("filterOffCommand")
public class FilterOffCommand implements Command {
    private static final String REDIRECT_NEWS_LIST_PAGE = "redirect:/newses";
    @Autowired
    private SessionAction sessionAction;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ControllerException {
        sessionAction.putSearchCriteria(request, null);
        return REDIRECT_NEWS_LIST_PAGE;
    }
}
