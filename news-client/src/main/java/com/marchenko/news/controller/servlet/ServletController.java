package com.marchenko.news.controller.servlet;

import com.marchenko.news.controller.command.Command;
import com.marchenko.news.controller.command.CommandStorage;
import com.marchenko.news.controller.ControllerException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Marchenko Vadim on 5/9/2016.
 */

@WebServlet(name = "ServletController", urlPatterns = {"/controller"})
public class ServletController extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final String REDIRECT_MARK = "redirect:";
    private static final String PAGE_404 = "/pages/jsp/404.jsp";
    private CommandStorage commandStorage;

    @Override
    public void init() throws ServletException {
        super.init();
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("spring-configuration.xml");
        commandStorage = context.getBean(CommandStorage.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        handleRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        handleRequest(req, resp);
    }

    private void handleRequest(HttpServletRequest request, HttpServletResponse response) {
        String nameCommand = takeNameCommand(request);
        Command command = commandStorage.takeCommand(nameCommand);
        String url = null;
        try {
            if (command != null) {
                url = command.execute(request, response);
                if (url != null) {
                    makeRedirectOrForward(url, request, response);
                }
            } else {
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(PAGE_404);
                dispatcher.forward(request, response);
            }
        } catch (ControllerException e) {
            LOGGER.error("Exception in controller layer with command="
                    + command, e);
        } catch (ServletException | IOException e) {
            LOGGER.error("Exception in makeRedirectOrForward() with url="
                    + url, e);
        } catch (Exception e){
            LOGGER.error("Unknown exception", e);
        }
    }

    private String takeNameCommand(HttpServletRequest request) {
        return request.getParameter("command");
    }

    private void makeRedirectOrForward(String url, HttpServletRequest request,
                                       HttpServletResponse response) throws ServletException, IOException {
        if (url.contains(REDIRECT_MARK)) {
            String newUrl = request.getContextPath() + url.replace(REDIRECT_MARK, "");
            response.sendRedirect(newUrl);
        } else {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
            dispatcher.forward(request, response);
        }
    }
}
