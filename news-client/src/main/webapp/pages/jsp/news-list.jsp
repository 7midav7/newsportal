<%@page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<link rel="stylesheet" href="${pageContext.request.contextPath}/pages/css/news-list.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/pages/css/news.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/pages/css/common.css">
</html>
<body>
<header>
    <c:import url="header.jsp"/>
    <hr>
</header>
<div class="clearfix">
    <nav>
        <div class="nav">
            <div>
                <a class="nav-item"
                   href="${pageContext.request.contextPath}/newses">News List</a>
            </div>
        </div>
    </nav>
    <main>
        <div class="news-list-wrapper">
            <div class="filter-wrapper clearfix">
                <div>
                    <form id="filterForm" action="filter-on">
                        <span>Author:</span>
                        <select id="author-select" name="authorId">
                            <option value="">Choose ...</option>
                            <c:forEach var="author" items="${authors}">
                                <option value="${author.id}"
                                        <c:if test="${author.id == filterAuthor}">selected</c:if>
                                >${author.name}</option>
                            </c:forEach>
                        </select>
                        <span>Tags:</span>
                        <select id="tags-select" name="tagIds" multiple>
                            <c:forEach var="tag" items="${filterTags}">
                                <option value="${tag.id}" selected>${tag.tagName}</option>
                            </c:forEach>
                            <c:forEach var="tag" items="${otherTags}">
                                <option value="${tag.id}">${tag.tagName}</option>
                            </c:forEach>
                        </select>
                    </form>
                </div>
                <div class="clearfix">
                    <button id="resetButton" onclick="window.open('${pageContext.
                            request.contextPath}/filter-off','_self')">Reset
                    </button>
                    <button id="filterButton" type="submit" form="filterForm">Filter</button>
                </div>
            </div>
            <hr>
            <c:forEach var="newsBlock" items="${newses}">
                <div class="news-wrapper clearfix">
                    <a class="news-title" href="view-news?id=<c:out value="${newsBlock.news.id}"/>">
                        <c:out value="${newsBlock.news.title}"/>
                    </a>
                    <span class="news-author">( by <c:out value="${newsBlock.author.name}"/> )</span>
                    <fmt:parseDate value="${newsBlock.news.modificationDate}" var="parsedDateFromString"
                                   pattern="yyyy-MM-dd" type="date"/>
                    <fmt:formatDate value="${parsedDateFromString}" var="parsedDate" pattern="MM/dd/yyyy" type="date"/>
                    <span class="news-date"><c:out value="${parsedDate}"/></span>
                    <p class="news-short-text"><c:out value="${newsBlock.news.shortText}"/></p>
                    <c:forEach var="tag" items="${newsBlock.tags}">
                        <span class="news-tag"><c:out value="${tag.tagName}"/></span>
                    </c:forEach>
                    <span class="comment-text">Comments(${fn:length(newsBlock.comments)})</span>
                </div>
            </c:forEach>
            <div class="pagination">
                <c:if test="${currentPage != 1}">
                    <button onclick="window.open('${pageContext.
                            request.contextPath}/newses?page=1','_self')">1
                    </button>
                </c:if>
                <c:if test="${( currentPage - 2 > 1) && (currentPage - 2 < pageCount ) }">
                    <button onclick="window.open('${pageContext.request.
                            contextPath}/newses?page=${currentPage - 2}','_self')">${currentPage - 2}</button>
                </c:if>
                <c:if test="${( currentPage - 1 > 1) && (currentPage - 1 < pageCount ) }">
                    <button onclick="window.open('${pageContext.request.
                            contextPath}/newses?page=${currentPage - 1}','_self')">${currentPage - 1}</button>
                </c:if>
                <button disabled>${currentPage}</button>
                <c:if test="${( currentPage + 1 > 1) && (currentPage + 1 < pageCount ) }">
                    <button onclick="window.open('${pageContext.request.
                            contextPath}/newses?page=${currentPage + 1}','_self')">${currentPage + 1}</button>
                </c:if>
                <c:if test="${( currentPage + 2 > 1) && (currentPage + 2 < pageCount) }">
                    <button onclick="window.open('${pageContext.request.
                            contextPath}/newses?page=${currentPage + 2}','_self')">${currentPage + 2}</button>
                </c:if>
                <c:if test="${currentPage != pageCount}">
                    <button onclick="window.open('${pageContext.request.
                            contextPath}/newses?page=${pageCount}','_self')">${pageCount}</button>
                </c:if>
            </div>
        </div>
    </main>
</div>
<footer>
    <hr>
    <c:import url="footer.jsp"/>
</footer>
</body>
