<%@page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<link rel="stylesheet" href="${pageContext.request.contextPath}/pages/css/news-list.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/pages/css/news.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/pages/css/common.css">
<script src="${pageContext.request.contextPath}/pages/js/comment.js"></script>
</html>
<body>
<header>
    <c:import url="header.jsp"/>
    <hr>
</header>
<div class="clearfix">
    <nav>
        <div class="nav">
            <div>
                <a class="nav-item"
                   href="${pageContext.request.contextPath}/newses">News List</a>
            </div>
        </div>
    </nav>
    <main>
        <div class="news-content">
            <span class="news-title"><c:out value="${post.news.title}"/></span>
            <span class="news-author">( by <c:out value="${post.author.name}"/> )</span>
            <fmt:parseDate value="${post.news.modificationDate}" var="parsedDateFromString" pattern="yyyy-MM-dd" type="date"/>
            <fmt:formatDate value="${parsedDateFromString}" var="parsedDate" pattern="MM/dd/yyyy" type="date"/>
            <span class="news-date"><c:out value="${parsedDate}"/></span>
            <div class="news-full-text"><c:out value="${post.news.fullText}"/></div>
            <div>
                <c:forEach var="tag" items="${post.tags}">
                    <span class="news-tag"><c:out value="${tag.tagName}"/></span>
                </c:forEach>
            </div>
            <div class="comments-section">
                <c:forEach var="comment" items="${post.comments}">
                    <div class="comment">
                        <div class="comment-date-wrapper clearfix">
                            <div class="comment-date">
                                <fmt:parseDate value="${comment.creationDate}" var="parsedDateFromString" pattern="yyyy-MM-dd" type="date"/>
                                <fmt:formatDate value="${parsedDateFromString}" var="parsedDate" pattern="MM/dd/yyyy" type="date"/>
                                <c:out value="${parsedDate}"/>
                            </div>
                        </div>
                        <div><c:out value="${comment.text}"/></div>
                    </div>
                </c:forEach>
                <textarea class="writing-comment-field" placeholder="Type a comment..."></textarea>
                <div class="clearfix">
                    <button class="send-button" onclick="postComment(${post.news.id})">Post</button>
                </div>
            </div>
        </div>
    </main>
</div>
<footer>
    <hr>
    <c:import url="footer.jsp"/>
</footer>
</body>