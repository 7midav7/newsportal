var NAME_COMMENT_DIV = "comment";

function postComment(id){
    var commentDiv = document.createElement("div");
    var commentDateWrapperDiv = document.createElement("div");
    var commentDateDiv = document.createElement("div");
    var commentText = document.getElementsByClassName("writing-comment-field").item(0).value;
    var writingCommentField = document.getElementsByClassName("writing-comment-field").item(0);
    var commentTextNode = document.createTextNode(commentText);
    var pendingTextNode = document.createTextNode("pending...");

    commentDiv.className = "comment";
    commentDateWrapperDiv.className = "comment-date-wrapper clearfix";
    commentDateDiv.className = "comment-date";

    document.getElementsByClassName("comments-section").item(0).insertBefore(commentDiv, writingCommentField);
    commentDiv.appendChild(commentDateWrapperDiv);
    commentDiv.appendChild(commentTextNode);
    commentDateWrapperDiv.appendChild(commentDateDiv);
    commentDateDiv.appendChild(pendingTextNode);

    commentDateDiv.style.color = "#dd0";
    writingCommentField.value = "";
    sendRequestPostingComment(id, commentText, commentDateDiv);
}

function sendRequestPostingComment(id, text, statusNode){
    var xhr = new XMLHttpRequest();
    xhr.open("GET","add-comment?newsId=" + encodeURI(id) + "&text=" + encodeURI(text));
    xhr.onreadystatechange = function () {
        if (this.readyState != 4) return;
        if (this.status == 200) {
            var status = parseStatusFromResponse(xhr);
            statusNode.innerHTML = status;
            statusNode.style.color = "#000";
        }
    };
    xhr.send();
}

function parseStatusFromResponse(xmlHttpRequest){
    var comment = JSON.parse(xmlHttpRequest.responseText);
    if (comment.id != null){
        var date = comment.creationDate;
        var month = date.monthValue;
        var day = date.dayOfMonth;
        if (month.toString().length == 1){
            month = "0" + month;
        }
        if (day.toString().length == 1){
            day = "0" + day;
        }
        return month + "/" + day + "/" + date.year;
    } else {
        return comment.text;
    }
}