package com.marchenko.news.dao.interfaces;

import com.marchenko.news.exception.DaoException;
import com.marchenko.news.entity.Entity;

import java.util.Optional;

/**
 * Main interface with 4 basic CRUD operations
 *
 * @param <T> type of entity which is located in database
 */
public interface CrudDao<T extends Entity> {
    /** Creates entity in database
     *
     * @param entity
     * @return Optional of generated id by database
     * @throws DaoException
     */
    Optional<Long> create(T entity) throws DaoException;

    /** Reads entity from database by id
     *
     * @param id - id of entity
     * @return Optional of entity
     * @throws DaoException
     */
    Optional<T> read(Long id) throws DaoException;

    /** Updates entity in database using id of entity
     *
     * @param entity
     * @throws DaoException
     */
    void update(T entity) throws DaoException;

    /** Deletes entity from database
     *
     * @param id
     * @throws DaoException
     */
    void delete(Long id) throws DaoException;
}
