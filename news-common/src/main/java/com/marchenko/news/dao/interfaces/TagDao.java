package com.marchenko.news.dao.interfaces;

import com.marchenko.news.entity.Tag;
import com.marchenko.news.exception.DaoException;

import java.util.List;

/**
 * Provides access to Tag entities in database
 *
 * @author Marchanka Vadzim
 */
public interface TagDao extends CrudDao<Tag> {
    /** Finds all tags
     *
     * @return
     * @throws DaoException
     */
    List<Tag> findAll() throws DaoException;

    /** Detaches tag from all newses
     *
     * @param tagId
     * @throws DaoException
     */
    void detachTag(Long tagId) throws DaoException;
}
