package com.marchenko.news.dao.impl;

import com.marchenko.news.dao.interfaces.UserDao;
import com.marchenko.news.entity.User;
import com.marchenko.news.exception.DaoException;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 4/6/2016.
 */

@Component("userDao")
class UserDaoImpl implements UserDao{
    private static final String SQL_CREATE_QUERY = "INSERT INTO USERS (usrs_name, usrs_login, usrs_password) VALUES(?, ?, ?)";
    private static final String SQL_READ_BY_ID_QUERY = "SELECT usrs_name, usrs_login, usrs_password FROM USERS WHERE usrs_id = ?";
    private static final String SQL_UPDATE_QUERY = "UPDATE USERS SET usrs_name=?, usrs_login=?, usrs_password=? WHERE usrs_id = ?";
    private static final String SQL_DELETE_QUERY = "DELETE FROM USERS WHERE USRS_ID=?";
    private static final String NAME_COLUMN = "usrs_id";
    @Autowired
    private BasicDataSource dataSource;


    @Override
    public Optional<Long> create(User user) throws DaoException {
        String[] generatedColumns = {NAME_COLUMN};
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement =
                     connection.prepareStatement(SQL_CREATE_QUERY, generatedColumns)) {
            statement.setString(1, user.getName());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPassword());
            statement.executeUpdate();
            try (ResultSet keys = statement.getGeneratedKeys()) {
                keys.next();
                long id = keys.getLong(1);
                return id != 0 ? Optional.of(id) : Optional.empty();
            }
        } catch (SQLException e) {
            throw new DaoException("create(user) method with user=" + user, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Optional<User> read(Long id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try(PreparedStatement statement = connection.prepareStatement(SQL_READ_BY_ID_QUERY)){
            statement.setLong(1, id);
            try(ResultSet set = statement.executeQuery()){
                if (set.next()) {
                    String name = set.getString(1);
                    String login = set.getString(2);
                    String password = set.getString(3);
                    User user = new User();
                    user.setId(id);
                    user.setName(name);
                    user.setLogin(login);
                    user.setPassword(password);
                    return Optional.of(user);
                } else {
                    return Optional.empty();
                }
            }
        } catch (SQLException e) {
            throw new DaoException("Connection error in read(user) method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void update(User user) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try(PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_QUERY)){
            statement.setString(1, user.getName());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPassword());
            statement.setLong(4, user.getId());
            int numberUpdatedRows = statement.executeUpdate();
            if (numberUpdatedRows != 1) {
                throw new DaoException("Zero or more than one user are updated" + user);
            }
        } catch (SQLException e) {
            throw new DaoException("Connection error in update(user) method", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void delete(Long id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try(PreparedStatement statement = connection.prepareStatement(SQL_DELETE_QUERY)){
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Connection error in delete(user) method", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }
}
