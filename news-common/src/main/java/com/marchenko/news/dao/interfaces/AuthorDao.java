package com.marchenko.news.dao.interfaces;

import com.marchenko.news.entity.Author;
import com.marchenko.news.exception.DaoException;

import java.util.List;

/**
 * Provides access to Author entities in database
 *
 * @author Marchanka Vadzim
 */
public interface AuthorDao extends CrudDao<Author> {
    /**
     * Finds all authors in database
     *
     * @return List of Author entities
     *
     * @throws DaoException
     */
    List<Author> findAll() throws DaoException;
}
