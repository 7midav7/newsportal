package com.marchenko.news.dao.impl;

import com.marchenko.news.dao.util.JdbcFunction;
import com.marchenko.news.exception.DaoException;
import com.marchenko.news.dao.interfaces.CommentDao;
import com.marchenko.news.entity.Comment;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component("commentDao")
class CommentDaoImpl implements CommentDao {
    private static final String INSERT_QUERY = "INSERT INTO comments " +
            " (comm_id, comm_news_id, comm_text, comm_creation_date) " +
            " VALUES(comm_seq.nextval, ?, ?, ?)";
    private static final String UPDATE_QUERY = "UPDATE comments " +
            " SET comm_news_id=?, comm_text=?, comm_creation_date=? WHERE comm_id=?";
    private static final String DELETE_QUERY = "DELETE FROM comments WHERE comm_id=?";
    private static final String FIND_BY_ID_QUERY = "SELECT comm_news_id, comm_text, " +
            " comm_creation_date FROM comments WHERE comm_id = ?";
    private static final String REMOVE_BY_NEWS_ID_QUERY = "DELETE FROM comments WHERE " +
            " comm_news_id=?";
    private static final String FIND_BY_NEWS_ID_QUERY = "SELECT comm_id, comm_text, " +
            " comm_creation_date FROM comments WHERE comm_news_id = ?";
    private static final String SQL_NAME_ID_COLUMN = "comm_id";
    @Autowired
    private BasicDataSource dataSource;

    @Override
    public Optional<Long> create(Comment entity) throws DaoException {
        String[] generatedColumns = {SQL_NAME_ID_COLUMN};
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement =
                     connection.prepareStatement(INSERT_QUERY, generatedColumns)) {
            statement.setLong(1, entity.getNewsId());
            statement.setString(2, entity.getText());
            JdbcFunction.setTimestamp(statement, 3, entity.getCreationDate());
            statement.executeUpdate();
            try (ResultSet keys = statement.getGeneratedKeys()) {
                keys.next();
                long id = keys.getLong(1);
                return id != 0 ? Optional.of(id) : Optional.empty();
            }
        } catch (SQLException e) {
            throw new DaoException("Connection error in create(comment) method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void update(Comment entity) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_QUERY)) {
            statement.setLong(1, entity.getNewsId());
            statement.setString(2, entity.getText());
            JdbcFunction.setTimestamp(statement, 3, entity.getCreationDate());
            statement.setLong(4, entity.getId());
            int numberUpdatedRows = statement.executeUpdate();
            if (numberUpdatedRows != 1) {
                throw new DaoException("Zero or more than one comment are updated" + entity);
            }
        } catch (SQLException e) {
            throw new DaoException("Connection error in updateNews(comment) method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

    }

    @Override
    public void delete(Long id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(DELETE_QUERY)) {
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Connection error in delete(comment) method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Optional<Comment> read(Long id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(FIND_BY_ID_QUERY)) {
            statement.setLong(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                return resultSet.next() ? Optional.of(parseWithId(resultSet, id)) : Optional.empty();
            }
        } catch (SQLException e) {
            throw new DaoException("Connection error in read(comment) method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void removeByNewsId(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(REMOVE_BY_NEWS_ID_QUERY)) {
            statement.setLong(1, newsId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Connection error in removeByNewsId ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<Comment> findByNewsId(Long newsId) throws DaoException {
        List<Comment> result = new ArrayList<>();
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(FIND_BY_NEWS_ID_QUERY)) {
            statement.setLong(1, newsId);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Comment comment = parseWithNewsId(resultSet, newsId);
                    result.add(comment);
                }
            }
        } catch (SQLException e) {
            throw new DaoException("Connection error in findByNewsId ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return result;
    }

    private Comment parseWithId(ResultSet resultSet, Long id) throws SQLException {
        Long newsId = resultSet.getLong(1);
        String text = resultSet.getString(2);
        LocalDateTime creationDate = JdbcFunction.takeTimestamp(resultSet, 3).get();
        Comment comment = new Comment(text);
        comment.setText(text);
        comment.setCreationDate(creationDate);
        comment.setId(id);
        comment.setNewsId(newsId);
        return comment;
    }

    private Comment parseWithNewsId(ResultSet resultSet, Long newsId) throws SQLException {
        Long id = resultSet.getLong(1);
        String text = resultSet.getString(2);
        LocalDateTime creationDate = JdbcFunction.takeTimestamp(resultSet, 3).get();
        Comment comment = new Comment(text);
        comment.setText(text);
        comment.setCreationDate(creationDate);
        comment.setId(id);
        comment.setNewsId(newsId);
        return comment;
    }
}
