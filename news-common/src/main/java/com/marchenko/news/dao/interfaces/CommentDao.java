package com.marchenko.news.dao.interfaces;

import com.marchenko.news.exception.DaoException;
import com.marchenko.news.entity.Comment;

import java.util.List;

/**
 * Provides access to Comment entities in database
 *
 * @author Marchanka Vadzim
 */
public interface CommentDao extends CrudDao<Comment> {
    /**
     * Removes comments with news id = newsId.
     *
     * @param newsId
     * @throws DaoException
     */
    void removeByNewsId(Long newsId) throws DaoException;

    /** Finds comments with news id = newsId
     *
     * @param newsId
     * @return List of Comment entity
     * @throws DaoException
     */
    List<Comment> findByNewsId(Long newsId) throws DaoException;
}
