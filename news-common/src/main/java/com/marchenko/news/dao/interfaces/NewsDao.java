package com.marchenko.news.dao.interfaces;

import com.marchenko.news.exception.DaoException;
import com.marchenko.news.entity.*;

import java.util.List;


/**
 * Provides access to News entities in database.
 *
 * @author Marchanka Vadzim
 */
public interface NewsDao extends CrudDao<News> {
    /** Attaches tag with tagId to news with newsId
     *
     * @param newsId
     * @param tagId
     * @throws DaoException
     */
    void attachTag(Long newsId, Long tagId) throws DaoException;

    /** Attaches author with authorId to news with newsId
     *
     * @param newsId
     * @param authorId
     * @throws DaoException
     */
    void attachAuthor(Long newsId, Long authorId) throws DaoException;

    /** Detaches tag with tagId from news with newsId
     *
     * @param newsId
     * @param idTag
     * @throws DaoException
     */
    void detachTag(Long newsId, Long idTag) throws DaoException;

    /** Detaches author with authorId from news with newsId
     *
     * @param newsId
     * @param idAuthor
     * @throws DaoException
     */
    void detachAuthor(Long newsId, Long idAuthor) throws DaoException;

    /** Detaches all tags from news with newsId
     *
     * @param newsId
     * @throws DaoException
     */
    void detachAllTags(Long newsId) throws DaoException;

    /** Detaches all authors from news with newsId
     *
     * @param newsId
     * @throws DaoException
     */
    void detachAllAuthors(Long newsId) throws DaoException;

    /** Finds all newses in database
     *
     * @return List of News entity
     * @throws DaoException
     */
    List<News> findAll() throws DaoException;

    /** Finds all attached tags
     *
     * @param newsId
     * @return List of Tag entity
     * @throws DaoException
     */
    List<Tag> findAttachedTags(Long newsId) throws DaoException;

    /** Finds all attached authors
     *
     * @param newsId
     * @return List of Author entity
     * @throws DaoException
     */
    List<Author> findAttachedAuthors(Long newsId) throws DaoException;

    /** Finds all newses and sorts them by comment count, returns
     * result in range[firstIndex, lastIndex]
     *
     * @param firstIndex
     * @param lastIndex
     * @return List of Comments in range[firstIndex, lastIndex]
     * @throws DaoException
     */
    List<News> takeMostCommentedNews(Long firstIndex, Long lastIndex) throws DaoException;

    /** Returns count of all newses
     *
     * @return count
     * @throws DaoException
     */
    long countNewses() throws DaoException;

    /** Finds all newses by SearchCriteria, returns result in
     * range[firstIndex, lastIndex].
     *
     * @param searchCriteria
     * @param firstIndex
     * @param lastIndex
     * @return List of News entity
     * @throws DaoException
     */
    List<News> findBySearchCriteria(SearchCriteria searchCriteria, Long firstIndex,
                                    Long lastIndex) throws DaoException;

    /** Returns the count of newses which are found by searchCriteria
     *
     * @param searchCriteria
     * @return Count newses
     * @throws DaoException
     */
    Long countNewsesBySearchCriteria(SearchCriteria searchCriteria) throws DaoException;
}
