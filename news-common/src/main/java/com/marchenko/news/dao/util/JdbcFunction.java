package com.marchenko.news.dao.util;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 2/16/2016.
 */
public class JdbcFunction {

    private JdbcFunction() {

    }

    /** Sets Timestamp entity which is represented by LocalDateTime entity
     * in PreparedStatement
     *
     * @param statement
     * @param position
     * @param time
     * @throws SQLException
     */
    public static void setTimestamp(PreparedStatement statement, int position, LocalDateTime time) throws SQLException {
        if (time != null) {
            Timestamp timestamp = Timestamp.valueOf(time);
            statement.setTimestamp(position, timestamp);
        } else {
            statement.setNull(position, Types.TIMESTAMP);
        }
    }

    /** Takes LocalDateTime entity from ResultSet
     *
     * @param resultSet
     * @param position
     * @return Optional of LocalDateTime entity
     * @throws SQLException
     */
    public static Optional<LocalDateTime> takeTimestamp(ResultSet resultSet, int position) throws SQLException {
        Timestamp timestamp = resultSet.getTimestamp(position);
        return timestamp != null ? Optional.of(timestamp.toLocalDateTime()) : Optional.empty();
    }

    /** Sets Date in PreparedStatement by LocalDateTime
     *
     * @param statement
     * @param position
     * @param time
     * @throws SQLException
     */
    public static void setDate(PreparedStatement statement, int position, LocalDateTime time) throws SQLException {
        if (time != null) {
            Date date = Date.valueOf(time.toLocalDate());
            statement.setDate(position, date);
        } else {
            statement.setNull(position, Types.DATE);
        }
    }

    /** Takes LocalDateTime object from ResultSet
     *
     * @param resultSet
     * @param position
     * @return
     * @throws SQLException
     */
    public static Optional<LocalDateTime> takeDate(ResultSet resultSet, int position) throws SQLException {
        Date date = resultSet.getDate(position);
        return date != null ? Optional.of(date.toLocalDate().atTime(0, 0)) : Optional.empty();
    }
}
