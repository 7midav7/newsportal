package com.marchenko.news.dao.impl;

import com.marchenko.news.exception.DaoException;
import com.marchenko.news.dao.interfaces.TagDao;
import com.marchenko.news.entity.Tag;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component("tagDao")
class TagDaoImpl implements TagDao {
    private static final String SQL_INSERT_QUERY = "INSERT INTO tags(tags_id, tags_name) "
            + " VALUES(tags_seq.nextval, ?)";
    private static final String SQL_UPDATE_QUERY = "UPDATE tags SET tags_name = ? WHERE tags_id = ?";
    private static final String SQL_DELETE_QUERY = "DELETE FROM tags WHERE tags_id = ?";
    private static final String SQL_FIND_BY_ID_QUERY = "SELECT tags_name FROM tags WHERE tags_id = ?";
    private static final String SQL_FIND_ALL_QUERY = "SELECT tags_name, tags_id FROM tags";
    private static final String SQL_DETACH_TAG_BY_ID = "DELETE FROM news_tags WHERE neta_tag_id=?";
    private static final String SQL_ID_NAME_COLUMN = "tags_id";
    @Autowired
    private BasicDataSource dataSource;

    @Override
    public Optional<Long> create(Tag entity) throws DaoException {
        String[] generatedColumns = {SQL_ID_NAME_COLUMN};
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_INSERT_QUERY, generatedColumns)) {
            statement.setString(1, entity.getTagName());
            statement.executeUpdate();
            try (ResultSet keys = statement.getGeneratedKeys()) {
                keys.next();
                long id = keys.getLong(1);
                return id != 0 ? Optional.of(id) : Optional.empty();
            }
        } catch (SQLException e) {
            throw new DaoException("Connection error in create(tag) method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void update(Tag entity) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_QUERY)) {
            statement.setString(1, entity.getTagName());
            statement.setLong(2, entity.getId());
            int numberUpdatedRows = statement.executeUpdate();
            if (numberUpdatedRows != 1) {
                throw new DaoException("Zero or more than one tag are updated" + entity);
            }
        } catch (SQLException e) {
            throw new DaoException("Connection error in updateNews(tag) method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void delete(Long id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_DELETE_QUERY)) {
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Delete(tag) method with id=" + id, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

    }

    @Override
    public Optional<Tag> read(Long id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID_QUERY)) {
            statement.setLong(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                return resultSet.next() ? Optional.of(parse(resultSet, id)) : Optional.empty();
            }
        } catch (SQLException e) {
            throw new DaoException("Connection error in read(tag) method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<Tag> findAll() throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL_QUERY)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                List<Tag> tags = new ArrayList<>();
                while (resultSet.next()){
                    long id = resultSet.getLong(2);
                    tags.add(parse(resultSet, id));
                }
                return tags;
            }
        } catch (SQLException e) {
            throw new DaoException("Connection error in read(tag) method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void detachTag(Long tagId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_DETACH_TAG_BY_ID)) {
            statement.setLong(1, tagId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("DetachTag method with id" + tagId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    private Tag parse(ResultSet set, Long id) throws SQLException {
        String name = set.getString(1);
        Tag tag = new Tag(name);
        tag.setId(id);
        return tag;
    }
}
