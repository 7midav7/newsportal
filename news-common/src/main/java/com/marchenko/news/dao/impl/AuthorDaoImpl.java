package com.marchenko.news.dao.impl;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.marchenko.news.dao.util.JdbcFunction;
import com.marchenko.news.exception.DaoException;
import com.marchenko.news.dao.interfaces.AuthorDao;
import com.marchenko.news.entity.Author;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

@Component("authorDao")
class AuthorDaoImpl implements AuthorDao {
    private static final String SQL_INSERT_QUERY = "INSERT INTO authors (auth_name, auth_expired) "
            + "VALUES(?, ?)";
    private static final String SQL_UPDATE_QUERY = "UPDATE authors SET auth_name=?, auth_expired=? where auth_id=?";
    private static final String SQL_DELETE_QUERY = "DELETE FROM authors WHERE auth_id=?";
    private static final String SQL_FIND_BY_ID_QUERY = "SELECT auth_name, auth_expired FROM authors WHERE auth_id = ?";
    private static final String SQL_FIND_ALL_QUERY = "SELECT auth_name, auth_expired, auth_id FROM authors";
    private static final String SQL_ID_NAME_COLUMN = "auth_id";
    @Autowired
    private BasicDataSource dataSource;

    @Override
    public Optional<Long> create(Author entity) throws DaoException {
        String[] generatedColumns = {SQL_ID_NAME_COLUMN};
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement =
                     connection.prepareStatement(SQL_INSERT_QUERY, generatedColumns)) {
            statement.setString(1, entity.getName());
            JdbcFunction.setTimestamp(statement, 2, entity.getTimeExpired());
            statement.executeUpdate();
            try (ResultSet keys = statement.getGeneratedKeys()) {
                keys.next();
                long id = keys.getLong(1);
                return id != 0 ? Optional.of(id) : Optional.empty();
            }
        } catch (SQLException e) {
            throw new DaoException("Connection error in create(author) method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void update(Author entity) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_QUERY)) {
            statement.setString(1, entity.getName());
            JdbcFunction.setTimestamp(statement, 2, entity.getTimeExpired());
            statement.setLong(3, entity.getId());
            int numberUpdatedRows = statement.executeUpdate();
            if (numberUpdatedRows != 1) {
                throw new DaoException("Zero or more than one author are updated" + entity);
            }
        } catch (SQLException e) {
            throw new DaoException("Connection error in updateNews(author) method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

    }

    @Override
    public void delete(Long id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_DELETE_QUERY)) {
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Connection error in delete(author) method", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Optional<Author> read(Long id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID_QUERY)) {
            statement.setLong(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                return resultSet.next() ?
                        Optional.of(parse(resultSet, id)) : Optional.empty();
            }
        } catch (SQLException e) {
            throw new DaoException("Connection error in read(author) method", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<Author> findAll() throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL_QUERY)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                ArrayList<Author> authors = new ArrayList<>();
                while ( resultSet.next() ){
                    long id = resultSet.getLong(3);
                    authors.add(parse(resultSet, id));
                }
                return authors;
            }
        } catch (SQLException e) {
            throw new DaoException("Connection error in read(author) method", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    private Author parse(ResultSet resultSet, Long id) throws SQLException {
        String name = resultSet.getString(1);
        Author author = new Author(name);
        author.setId(id);
        Optional<LocalDateTime> optionalTime = JdbcFunction.takeTimestamp(resultSet, 2);
        if (optionalTime.isPresent()) {
            author.setTimeExpired(optionalTime.get());
        }
        return author;
    }
}
