package com.marchenko.news.dao.interfaces;

import com.marchenko.news.entity.User;

/**
 * Created by Marchenko Vadim on 4/6/2016.
 */

/**
 * Provides access to User entities in database
 *
 * @author Marchanka Vadzim
 */
public interface UserDao extends CrudDao<User>{
}
