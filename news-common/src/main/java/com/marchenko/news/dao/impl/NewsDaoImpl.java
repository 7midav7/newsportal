package com.marchenko.news.dao.impl;

import com.marchenko.news.dao.util.JdbcFunction;
import com.marchenko.news.exception.DaoException;
import com.marchenko.news.dao.interfaces.NewsDao;
import com.marchenko.news.entity.Author;
import com.marchenko.news.entity.News;
import com.marchenko.news.entity.SearchCriteria;
import com.marchenko.news.entity.Tag;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component("newsDao")
class NewsDaoImpl implements NewsDao {
    private static final String SQL_INSERT_QUERY = "INSERT INTO news " +
            " (news_id, news_title, news_short_text, news_full_text, " +
            " news_creation_date, news_modification_date) " +
            " VALUES(news_seq.nextval, ?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE_QUERY = "UPDATE news SET " +
            " news_title=?, news_short_text=?, news_full_text=?, " +
            " news_creation_date=?, news_modification_date=? " +
            " where news_id=?";
    private static final String SQL_DELETE_QUERY = "DELETE FROM news WHERE news_id = ?";
    private static final String SQL_FIND_BY_ID_QUERY = "SELECT news_title, news_short_text, " +
            " news_full_text, news_creation_date, news_modification_date FROM news " +
            " WHERE news_id = ?";
    private static final String SQL_FIND_ALL_QUERY = "SELECT news_title, news_short_text, " +
            " news_full_text, news_creation_date, news_modification_date, news_id FROM news";
    private static final String SQL_ADD_AUTHOR_QUERY =
            "INSERT INTO author_news (aune_news_id, aune_author_id) VALUES(?, ?)";
    private static final String SQL_ADD_TAG_QUERY =
            "INSERT INTO news_tags (neta_news_id, neta_tag_id) VALUES(?, ?)";
    private static final String SQL_REMOVE_AUTHOR_QUERY =
            "DELETE FROM author_news WHERE aune_news_id=? AND aune_author_id=?";
    private static final String SQL_REMOVE_TAG_QUERY =
            "DELETE FROM news_tags WHERE neta_news_id=? AND neta_tag_id=?";
    private static final String SQL_REMOVE_ALL_TAGS_QUERY = "DELETE FROM news_tags WHERE neta_news_id = ?";
    private static final String SQL_REMOVE_ALL_AUTHORS_QUERY = "DELETE FROM author_news WHERE aune_news_id = ?";
    private static final String SQL_FIND_TAGS_QUERY = "SELECT tags_id, tags_name FROM tags " +
            " INNER JOIN news_tags ON tags_id=neta_tag_id " +
            " WHERE neta_news_id=?";
    private static final String SQL_FIND_AUTHORS_QUERY = "SELECT auth_id, auth_name, auth_expired " +
            " FROM author_news " +
            " INNER JOIN authors ON aune_author_id = auth_id " +
            " WHERE aune_news_id=?";
    private static final String SQL_COUNT_NEWS_QUERY = "SELECT count(*) FROM news";
    private static final String SQL_FIND_MOST_COMMENTED_QUERY =
            " SELECT news_title, news_short_text, news_full_text, news_creation_date, news_modification_date, news_id FROM( " +
                    " SELECT news_title, news_short_text, news_full_text, news_creation_date, news_modification_date, news_id, ROWNUM as row_number FROM( " +
                    " SELECT news_title, news_short_text, news_full_text, news_creation_date, news_modification_date, news_id FROM( " +
                    " SELECT news_title, news_short_text, news_full_text, news_creation_date, news_modification_date, news_id, " +
                    " CASE WHEN cnt IS NULL THEN 0 ELSE cnt END AS cnt  FROM news " +
                    " LEFT JOIN ( SELECT comm_news_id, count(*) AS cnt FROM comments " +
                    "               GROUP BY comm_news_id " +
                    "               ORDER BY count(*) ) " +
                    " ON news_id = comm_news_id ) " +
                    " ORDER BY cnt DESC ) )" +
                    " WHERE row_number>=? AND row_number<=? ";
    private static final String FIND_TEMPLATE = "SELECT news_title, news_short_text, " +
            " news_full_text, news_creation_date, news_modification_date, news_id FROM news ";
    private static final String NEWS_ID_FINDING_BY_AUTHOR_QUERY = "SELECT aune_news_id AS newsId FROM author_news " +
            " WHERE aune_author_id = ?";
    private static final String NEWS_ID_FINDING_BY_TAG_QUERY = "SELECT neta_news_id as newsId FROM NEWS_TAGS WHERE neta_tag_id = ? ";
    private static final String SQL_ID_NAME_COLUMN = "news_id";
    private static final String BEGINNING_SEARCHING_FILTER =
            " SELECT news_title, news_short_text, news_full_text, news_creation_date, news_modification_date, news_id FROM( " +
                    " SELECT news_title, news_short_text, " +
                    " news_full_text, news_creation_date, news_modification_date, news_id, ROWNUM as current_line  FROM( " +
                    " SELECT news_title, news_short_text, " +
                    " news_full_text, news_creation_date, news_modification_date, news_id FROM news " +
                    " INNER JOIN ( " +
                    " SELECT newsId, MAX(row_num) as row_num FROM( " +
                    " SELECT newsId, ROW_NUMBER()OVER(PARTITION BY newsId ORDER BY newsId) as row_num FROM( ";
    private static final String ENDING_SEARCHING_FILTER = " ) ) " +
            " GROUP BY newsId ) " +
            " ON newsId = news_id " +
            " ORDER BY row_num DESC " +
            " ) ) WHERE current_line>= ? AND current_line<=? ";
    private static final String BEGINNING_COUNTER_FILTER = " SELECT count(*)  FROM news " +
            " INNER JOIN ( " +
            " SELECT newsId, MAX(row_num) as row_num FROM( " +
            " SELECT newsId, ROW_NUMBER()OVER(PARTITION BY newsId ORDER BY newsId) as row_num FROM( ";
    private static final String ENDING_COUNTER_FILTER = " ) ) " +
            " GROUP BY newsId ) " +
            " ON newsId = news_id ";
    private static final String EMPTY_SELECT_QUERY = "SELECT * FROM news WHERE news_id<0";
    private static final long MAX_COUNT_ROWS = 1_000_000_000;

    @Autowired
    private BasicDataSource dataSource;


    @Override
    public Optional<Long> create(News entity) throws DaoException {
        String[] generatedColumns = {SQL_ID_NAME_COLUMN};
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (
                PreparedStatement statement = connection.prepareStatement(SQL_INSERT_QUERY, generatedColumns)) {
            statement.setString(1, entity.getTitle());
            statement.setString(2, entity.getShortText());
            statement.setString(3, entity.getFullText());
            JdbcFunction.setTimestamp(statement, 4, entity.getCreationDate());
            JdbcFunction.setDate(statement, 5, entity.getModificationDate());
            statement.executeUpdate();
            try (ResultSet keys = statement.getGeneratedKeys()) {
                keys.next();
                long id = keys.getLong(1);
                return id != 0 ? Optional.of(id) : Optional.empty();
            }
        } catch (SQLException e) {
            throw new DaoException("Connection error in create(news) method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void update(News entity) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_QUERY)) {
            statement.setString(1, entity.getTitle());
            statement.setString(2, entity.getShortText());
            statement.setString(3, entity.getFullText());
            JdbcFunction.setTimestamp(statement, 4, entity.getCreationDate());
            JdbcFunction.setDate(statement, 5, entity.getModificationDate());
            statement.setLong(6, entity.getId());
            int numberUpdatedRows = statement.executeUpdate();
            if (numberUpdatedRows != 1) {
                throw new DaoException("Zero or more than one newses are updated" + entity);
            }
        } catch (SQLException e) {
            throw new DaoException("Connection error in updateNews(news) method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void delete(Long id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_DELETE_QUERY)) {
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Connection error in delete(news) method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Optional<News> read(Long id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID_QUERY)) {
            statement.setLong(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                return resultSet.next() ? Optional.of(parseNews(resultSet, id)) : Optional.empty();
            }
        } catch (SQLException e) {
            throw new DaoException("Connection error in read(news) method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void attachTag(Long newsId, Long tagId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_ADD_TAG_QUERY)) {
            statement.setLong(1, newsId);
            statement.setLong(2, tagId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Connection error in attachTag method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void attachAuthor(Long newsId, Long authorId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_ADD_AUTHOR_QUERY)) {
            statement.setLong(1, newsId);
            statement.setLong(2, authorId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Connection error in attachAuthor method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void detachTag(Long newsId, Long tagId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_REMOVE_TAG_QUERY)) {
            statement.setLong(1, newsId);
            statement.setLong(2, tagId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Connection error in detachTag method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void detachAuthor(Long newsId, Long authorId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_REMOVE_AUTHOR_QUERY)) {
            statement.setLong(1, newsId);
            statement.setLong(2, authorId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Connection error in detachAuthor method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void detachAllTags(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_REMOVE_ALL_TAGS_QUERY)) {
            statement.setLong(1, newsId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Connection error in detachAllTags method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void detachAllAuthors(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_REMOVE_ALL_AUTHORS_QUERY)) {
            statement.setLong(1, newsId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Connection error in removeByNewsId method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<News> findAll() throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL_QUERY)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                return parseNewsList(resultSet);
            }
        } catch (SQLException e) {
            throw new DaoException("Connection error in findAll(news) method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<Tag> findAttachedTags(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_FIND_TAGS_QUERY)) {
            statement.setLong(1, newsId);
            try (ResultSet resultSet = statement.executeQuery()) {
                return parseTagsList(resultSet);
            }
        } catch (SQLException e) {
            throw new DaoException("Connection error in findAttachedTags method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<Author> findAttachedAuthors(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_FIND_AUTHORS_QUERY)) {
            statement.setLong(1, newsId);
            try (ResultSet resultSet = statement.executeQuery()) {
                return parseAuthorsList(resultSet);
            }
        } catch (SQLException e) {
            throw new DaoException("Connection error in findAttachedAuthors method ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public List<News> takeMostCommentedNews(Long firstIndex, Long lastIndex) throws DaoException {
        if (firstIndex.compareTo(lastIndex) > 1) {
            throw new DaoException("firstIndex=" + firstIndex + " is bigger than lastIndex=" + lastIndex);
        }
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_FIND_MOST_COMMENTED_QUERY)) {
            statement.setLong(1, firstIndex);
            statement.setLong(2, lastIndex);
            try (ResultSet resultSet = statement.executeQuery()) {
                return parseNewsList(resultSet);
            }
        } catch (SQLException e) {
            throw new DaoException("Connection error in takeMostCommentedNews ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public long countNewses() throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(SQL_COUNT_NEWS_QUERY)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                resultSet.next();
                return resultSet.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException("Connection error in countNewses ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<News> findBySearchCriteria(SearchCriteria searchCriteria, Long firstIndex, Long lastIndex) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement =
                     makeSearchingCriteriaStatement(connection, searchCriteria, firstIndex, lastIndex)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                return parseNewsList(resultSet);
            }
        } catch (SQLException e) {
            throw new DaoException("Connection error in findBySearchCriteria ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public Long countNewsesBySearchCriteria(SearchCriteria searchCriteria) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = makeCountingCriteriaStatement(connection, searchCriteria)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                resultSet.next();
                return resultSet.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException("SQL exception in countNewsesBySearchCriteria ", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    private PreparedStatement makeSearchingCriteriaStatement(Connection connection, SearchCriteria searchCriteria,
                                                             Long firstIndex, Long lastIndex) throws SQLException {
        StringBuilder query = new StringBuilder(BEGINNING_SEARCHING_FILTER);
        int additionsCounter = 0;
        additionsCounter = appendQueryWithAuthorInformation(query, searchCriteria.getAuthorId(), additionsCounter);
        appendQueryWithTagsInformation(query, searchCriteria.getTagIdList(), additionsCounter);
        query.append(ENDING_SEARCHING_FILTER);
        PreparedStatement statement = connection.prepareStatement(query.toString());
        int position = 0;
        position = fillStatementByAuthorId(statement, searchCriteria.getAuthorId(), position);
        position = fillStatementByTagsId(statement, searchCriteria.getTagIdList(), position);
        fillStatementByBorders(statement, firstIndex, lastIndex, position);
        return statement;
    }

    private PreparedStatement makeCountingCriteriaStatement(Connection connection,
                                                            SearchCriteria searchCriteria) throws SQLException {
        StringBuilder query = new StringBuilder(BEGINNING_COUNTER_FILTER);
        int additionsCounter = 0;
        additionsCounter = appendQueryWithAuthorInformation(query, searchCriteria.getAuthorId(), additionsCounter);
        appendQueryWithTagsInformation(query, searchCriteria.getTagIdList(), additionsCounter);
        query.append(ENDING_COUNTER_FILTER);
        PreparedStatement statement = connection.prepareStatement(query.toString());
        int position = 0;
        position = fillStatementByAuthorId(statement, searchCriteria.getAuthorId(), position);
        fillStatementByTagsId(statement, searchCriteria.getTagIdList(), position);
        return statement;
    }

    private int appendQueryWithAuthorInformation(StringBuilder query, Long authorId, int additionsCount) {
        if (authorId != null) {
            if (additionsCount > 0) {
                query.append(" UNION ALL ");
            }
            ++additionsCount;
            query.append(NEWS_ID_FINDING_BY_AUTHOR_QUERY);
        }
        return additionsCount;
    }

    private int appendQueryWithTagsInformation(StringBuilder query, List<Long> tagList, int additionsCount) {
        if (tagList == null || tagList.isEmpty()) {
            return additionsCount;
        }
        if (additionsCount > 0) {
            query.append(" UNION ALL ");
        }
        for (int i = 0; i < tagList.size(); ++i) {
            ++additionsCount;
            query.append(NEWS_ID_FINDING_BY_TAG_QUERY);
            if (i != tagList.size() - 1) {
                query.append(" UNION ALL ");
            }
        }
        return additionsCount;
    }

    private int fillStatementByAuthorId(PreparedStatement statement, Long authorId,
                                        int position) throws SQLException {
        if (authorId != null) {
            ++position;
            statement.setLong(position, authorId);
        }
        return position;
    }

    private int fillStatementByTagsId(PreparedStatement statement, List<Long> tagList,
                                      int position) throws SQLException {
        if (tagList == null || tagList.isEmpty()) {
            return position;
        }
        for (Long tagId : tagList) {
            ++position;
            statement.setLong(position, tagId);
        }
        return position;
    }

    private int fillStatementByBorders(PreparedStatement statement, Long leftBorder, Long rightBorder,
                                       int position) throws SQLException {
        ++position;
        statement.setLong(position, leftBorder);
        ++position;
        statement.setLong(position, rightBorder);
        return position;
    }


    private News parseNews(ResultSet resultSet, Long id) throws SQLException {
        String title = resultSet.getString(1);
        String shortText = resultSet.getString(2);
        String fullText = resultSet.getString(3);
        LocalDateTime creationDate = JdbcFunction.takeTimestamp(resultSet, 4).get();
        LocalDateTime modificationDate = JdbcFunction.takeDate(resultSet, 5).get();
        News news = new News(title, fullText);
        news.setShortText(shortText);
        news.setId(id);
        news.setTitle(title);
        news.setCreationDate(creationDate);
        news.setModificationDate(modificationDate);
        return news;
    }

    private List<Author> parseAuthorsList(ResultSet resultSet) throws SQLException {
        List<Author> authors = new ArrayList<>();
        while (resultSet.next()) {
            Long id = resultSet.getLong(1);
            String name = resultSet.getString(2);
            Optional<LocalDateTime> expiredOptional = JdbcFunction.takeTimestamp(resultSet, 3);
            Author author = new Author(name);
            author.setId(id);
            if (expiredOptional.isPresent()) {
                author.setTimeExpired(expiredOptional.get());
            }
            authors.add(author);
        }
        return authors;
    }

    private List<Tag> parseTagsList(ResultSet resultSet) throws SQLException {
        List<Tag> tags = new ArrayList<>();
        while (resultSet.next()) {
            Long id = resultSet.getLong(1);
            String name = resultSet.getString(2);
            Tag tag = new Tag(name);
            tag.setId(id);
            tags.add(tag);
        }
        return tags;
    }

    private List<News> parseNewsList(ResultSet resultSet) throws SQLException {
        List<News> newses = new ArrayList<>();
        while (resultSet.next()) {
            Long id = resultSet.getLong(6);
            News news = parseNews(resultSet, id);
            newses.add(news);
        }
        return newses;
    }
}
