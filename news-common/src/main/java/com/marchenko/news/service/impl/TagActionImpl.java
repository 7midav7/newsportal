package com.marchenko.news.service.impl;

import com.marchenko.news.exception.DaoException;
import com.marchenko.news.dao.interfaces.TagDao;
import com.marchenko.news.entity.Tag;
import com.marchenko.news.exception.ServiceException;
import com.marchenko.news.service.interfaces.TagAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 2/19/2016.
 */
@Component("tagAction")
class TagActionImpl implements TagAction {
    @Autowired
    private TagDao tagDao;

    @Override
    public void createTag(Tag tag) throws ServiceException {
        if (tag == null){
            throw new ServiceException("Tag is null");
        }
        try {
            Optional<Long> optionalId = tagDao.create(tag);
            if (optionalId.isPresent()){
                tag.setId(optionalId.get());
            } else {
                throw new ServiceException("Dao didn't create the tag = " + tag);
            }
        } catch (DaoException e) {
            throw new ServiceException("Exception in createTag method", e);
        }
    }

    @Override
    public void deleteTag(Long tagId) throws ServiceException {
        if (tagId == null){
            throw new ServiceException("Tag id is null");
        }
        try{
            tagDao.detachTag(tagId);
            tagDao.delete(tagId);
        } catch (DaoException e){
            throw new ServiceException("Exception in deleteTag method", e);
        }
    }

    @Override
    public void updateTag(Tag tag) throws ServiceException {
        if (tag == null){
            throw new ServiceException("Tag is null");
        }
        try {
            tagDao.update(tag);
        } catch (DaoException e) {
            throw new ServiceException("Exception in updateTag tag = " + tag, e);
        }
    }

    @Override
    public Optional<Tag> readTag(Long id) throws ServiceException {
        if (id == null){
            throw new ServiceException("id is null");
        }
        try {
            return tagDao.read(id);
        } catch (DaoException e) {
            throw new ServiceException("Exception in dao, id = " + id, e);
        }
    }

    @Override
    public List<Tag> findAll() throws ServiceException {
        try {
            return tagDao.findAll();
        } catch (DaoException e) {
            throw new ServiceException("Exception in findAll", e);
        }
    }
}
