package com.marchenko.news.service.impl;

import com.marchenko.news.exception.DaoException;
import com.marchenko.news.exception.ServiceException;
import com.marchenko.news.dao.interfaces.CommentDao;
import com.marchenko.news.dao.interfaces.NewsDao;
import com.marchenko.news.entity.*;
import com.marchenko.news.service.interfaces.NewsAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 2/17/2016.
 */
@Component("newsAction")
class NewsActionImpl implements NewsAction {
    @Autowired
    private CommentDao commentDao;
    @Autowired
    private NewsDao newsDao;

    @Override
    public Optional<Long> create(News news) throws ServiceException {
        if (news == null) {
            throw new ServiceException("News is null");
        }
        try {
            return newsDao.create(news);
        } catch (DaoException e) {
            throw new ServiceException("Error in create", e);
        }
    }

    @Override
    public void update(News news) throws ServiceException {
        if (news == null) {
            throw new ServiceException("News is null");
        }
        try {
            newsDao.update(news);
        } catch (DaoException e) {
            throw new ServiceException("Error in update", e);
        }
    }

    @Override
    public void delete(Long id) throws ServiceException {
        if (id == null) {
            throw new ServiceException("News id is null");
        }
        try {
            newsDao.delete(id);
        } catch (DaoException e) {
            throw new ServiceException("Error in delete id=" + id, e);
        }
    }

    @Override
    public Optional<News> read(Long id) throws ServiceException {
        if (id == null) {
            throw new ServiceException("News id is null");
        }
        try {
            return newsDao.read(id);
        } catch (DaoException e) {
            throw new ServiceException("id=" + id, e);
        }
    }

    @Override
    public void attachTag(Long newsId, Long tagId) throws ServiceException {
        if (newsId == null || tagId == null) {
            throw new ServiceException("News id or tag id is null( newsId=" +
                    newsId + " tagId=" + tagId + ")");
        }
        try {
            newsDao.attachTag(newsId, tagId);
        } catch (DaoException e) {
            throw new ServiceException("Error in attachTag(newsId=" + newsId
                    + ",tagId=" + tagId + ")", e);
        }
    }

    @Override
    public void attachAuthor(Long newsId, Long authorId) throws ServiceException {
        if (newsId == null || authorId == null) {
            throw new ServiceException("News id or tag id is null( newsId=" +
                    newsId + " tagId=" + authorId + ")");
        }
        try {
            newsDao.attachAuthor(newsId, authorId);
        } catch (DaoException e) {
            throw new ServiceException("Error in attachTag(newsId=" + newsId
                    + ",authorId=" + authorId + ")", e);
        }
    }

    @Override
    public void detachTag(Long newsId, Long tagId) throws ServiceException {
        if (newsId == null || tagId == null) {
            throw new ServiceException("News id or tag id is null( newsId=" +
                    newsId + " tagId=" + tagId + ")");
        }
        try {
            newsDao.detachTag(newsId, tagId);
        } catch (DaoException e) {
            throw new ServiceException("Error in attachTag(newsId=" + newsId
                    + ",tagId=" + tagId + ")", e);
        }
    }

    @Override
    public void detachAuthor(Long newsId, Long authorId) throws ServiceException {
        if (newsId == null || authorId == null) {
            throw new ServiceException("News id or tag id is null( newsId=" +
                    newsId + " tagId=" + authorId + ")");
        }
        try {
            newsDao.detachAuthor(newsId, authorId);
        } catch (DaoException e) {
            throw new ServiceException("Error in attachTag(newsId=" + newsId
                    + ",authorId=" + authorId + ")", e);
        }
    }

    @Override
    public void detachAllTags(Long newsId) throws ServiceException {
        if (newsId == null) {
            throw new ServiceException("News id is null");
        }
        try {
            newsDao.detachAllTags(newsId);
        } catch (DaoException e) {
            throw new ServiceException("News id=" + newsId, e);
        }
    }

    @Override
    public void detachAllAuthors(Long newsId) throws ServiceException {
        if (newsId == null) {
            throw new ServiceException("News id is null");
        }
        try {
            newsDao.detachAllAuthors(newsId);
        } catch (DaoException e) {
            throw new ServiceException("News id=" + newsId, e);
        }
    }

    @Override
    public List<News> findAll() throws ServiceException {
        try {
            return newsDao.findAll();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Tag> findAttachedTags(Long newsId) throws ServiceException {
        if (newsId == null) {
            throw new ServiceException("News id is null");
        }
        try {
            return newsDao.findAttachedTags(newsId);
        } catch (DaoException e) {
            throw new ServiceException("News id=" + newsId, e);
        }
    }

    @Override
    public List<Author> findAttachedAuthors(Long newsId) throws ServiceException {
        if (newsId == null) {
            throw new ServiceException("News id is null");
        }
        try {
            return newsDao.findAttachedAuthors(newsId);
        } catch (DaoException e) {
            throw new ServiceException("News id=" + newsId, e);
        }
    }

    @Override
    public List<News> takeMostCommentedNews(Long firstIndex, Long lastIndex) throws ServiceException {
        try {
            return newsDao.takeMostCommentedNews(firstIndex, lastIndex);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    @Override
    public List<News> findBySearchCriteria(SearchCriteria searchCriteria, Long firstIndex,
                                           Long lastIndex) throws ServiceException {
        if (searchCriteria == null) {
            throw new ServiceException("SearchCriteria is null");
        }
        try {
            return newsDao.findBySearchCriteria(searchCriteria, firstIndex, lastIndex);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    @Override
    public Long countNewsesBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException{
        if (searchCriteria == null) {
            throw new ServiceException("SearchCriteria is null");
        }
        try {
            return newsDao.countNewsesBySearchCriteria(searchCriteria);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    @Override
    public Long countNewses() throws ServiceException {
        try {
            return newsDao.countNewses();
        } catch (DaoException e) {
            throw new ServiceException("Error in tcountNewse method ", e);
        }
    }
}
