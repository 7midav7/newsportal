package com.marchenko.news.service.impl;

import com.marchenko.news.entity.*;
import com.marchenko.news.exception.ServiceException;
import com.marchenko.news.service.interfaces.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 3/11/2016.
 */
@Component("newsMessageAction")
class NewsPostActionImpl implements NewsPostAction {
    @Autowired
    private NewsAction newsAction;
    @Autowired
    private CommentAction commentAction;


    @Override
    @Transactional(rollbackFor = {ServiceException.class})
    public Long createNewsPost(NewsPost newsPost) throws ServiceException {
        if (newsPost == null) {
            throw new ServiceException("newsPost is null");
        }
        News news = newsPost.getNews();
        Author author = newsPost.getAuthor();
        List<Tag> tags = newsPost.getTags();
        Optional<Long> optionalNewsId = newsAction.create(news);
        if (optionalNewsId.isPresent()) {
            Long newsId = optionalNewsId.get();
            newsAction.attachAuthor(newsId, author.getId());
            for (Tag tag : tags) {
                newsAction.attachTag(newsId, tag.getId());
            }
            return optionalNewsId.get();
        } else {
            throw new ServiceException("Can't create a newsPost" + newsPost);
        }
    }

    @Override
    @Transactional(rollbackFor = {ServiceException.class})
    public void editNewsPostContent(NewsPost message) throws ServiceException {
        if (message == null) {
            throw new ServiceException("message is null");
        }
        News news = message.getNews();
        List<Tag> tags = message.getTags();
        Author author = message.getAuthor();
        newsAction.update(news);
        newsAction.detachAllTags(news.getId());
        newsAction.detachAllAuthors(news.getId());
        newsAction.attachAuthor(news.getId(), author.getId());
        for (Tag tag : tags) {
            newsAction.attachTag(news.getId(), tag.getId());
        }
    }

    @Override
    @Transactional(rollbackFor = {ServiceException.class})
    public void deleteNewsMessage(Long newsId) throws ServiceException {
        if (newsId == null) {
            throw new ServiceException("News id is null");
        }
        commentAction.deleteByNewsId(newsId);
        newsAction.detachAllTags(newsId);
        newsAction.detachAllAuthors(newsId);
        newsAction.delete(newsId);
    }

    @Override
    public Optional<NewsPost> readNews(Long newsId) throws ServiceException {
        if (newsId == null) {
            throw new ServiceException("News id is null");
        }
        NewsPost newsPost = new NewsPost();
        Optional<News> optionalNews = newsAction.read(newsId);
        if (optionalNews.isPresent()) {
            List<Author> authors = newsAction.findAttachedAuthors(newsId);
            List<Tag> tags = newsAction.findAttachedTags(newsId);
            List<Comment> comments = commentAction.findByNewsId(newsId);
            newsPost.setAuthor(authors.get(0));
            newsPost.setTags(tags);
            newsPost.setComments(comments);
            newsPost.setNews(optionalNews.get());
            return Optional.of(newsPost);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public List<NewsPost> convert(List<News> newses) throws ServiceException {
        List<NewsPost> result = new ArrayList<>();
        for (News news : newses) {
            NewsPost newsPost = new NewsPost();
            Long newsId = news.getId();
            List<Author> authors = newsAction.findAttachedAuthors(newsId);
            List<Tag> tags = newsAction.findAttachedTags(newsId);
            List<Comment> comments = commentAction.findByNewsId(newsId);
            newsPost.setAuthor(authors.get(0));
            newsPost.setTags(tags);
            newsPost.setNews(news);
            newsPost.setComments(comments);
            result.add(newsPost);
        }
        return result;
    }
}
