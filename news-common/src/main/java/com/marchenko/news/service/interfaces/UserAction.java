package com.marchenko.news.service.interfaces;

import com.marchenko.news.entity.User;
import com.marchenko.news.exception.ServiceException;

import java.util.Optional;

/**
 * Created by Marchenko Vadim on 4/6/2016.
 */

/** Provides operations with User entity
 *  @author Vadzim Marchanka
 */
public interface UserAction {
    /** Reads User entity from storage
     *
     * @param userId
     * @return
     * @throws ServiceException
     */
    Optional<User> readUser(Long userId) throws ServiceException;
}
