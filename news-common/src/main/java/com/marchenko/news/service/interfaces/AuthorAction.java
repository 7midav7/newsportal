package com.marchenko.news.service.interfaces;

import com.marchenko.news.entity.Author;
import com.marchenko.news.exception.ServiceException;

import java.util.List;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 2/19/2016.
 */

/** Provides operations with Author entity
 *  @author Vadzim Marchanka
 */
public interface AuthorAction {
    /** Creates Author entity in storage
     *
     * @param author is not null
     * @throws ServiceException
     */
    void createAuthor(Author author) throws ServiceException;

    /** Deletes Authors entity in storage
     *
     * @param id
     * @throws ServiceException
     */
    void deleteAuthor(Long id) throws ServiceException;

    /** Updates Author entity using his id in storage
     *
     * @param author
     * @throws ServiceException
     */
    void updateAuthor(Author author) throws ServiceException;

    /** Returns Author entity from storage using id
     *
     * @param id - id of entity
     * @return Optional of author entity
     * @throws ServiceException
     */
    Optional<Author> readAuthor(Long id) throws ServiceException;

    /** Finds all Author entities from storage
     *
     * @return List of Author entities
     * @throws ServiceException
     */
    List<Author> findAll() throws ServiceException;

    /** Finds all Author entities which are not expired
     *
     * @return List of Author entities
     * @throws ServiceException
     */
    List<Author> findNonExpiredAuthors() throws ServiceException;

    /** Makes Author entity expired
     *
     * @param id - id of entity
     * @throws ServiceException
     */
    void makeExpiredAuthor(Long id) throws ServiceException;
}
