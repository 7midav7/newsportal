package com.marchenko.news.service.impl;

import com.marchenko.news.dao.interfaces.UserDao;
import com.marchenko.news.entity.User;
import com.marchenko.news.exception.DaoException;
import com.marchenko.news.exception.ServiceException;
import com.marchenko.news.service.interfaces.UserAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Created by Marchenko Vadim on 4/6/2016.
 */
@Component
class UserActionImpl implements UserAction{
    @Autowired
    private UserDao userDao;


    @Override
    public Optional<User> readUser(Long userId) throws ServiceException {
        if (userId == null){
            throw new ServiceException("userId is null");
        }
        try {
            return userDao.read(userId);
        } catch (DaoException e) {
            throw new ServiceException("problem with userId = " + userId,e);
        }
    }
}
