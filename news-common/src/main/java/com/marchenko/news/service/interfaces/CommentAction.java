package com.marchenko.news.service.interfaces;

import com.marchenko.news.entity.Comment;
import com.marchenko.news.exception.ServiceException;

import java.util.List;
import java.util.Optional;

/** Provides operations with Comment entity
 *  @author Vadzim Marchanka
 */
public interface CommentAction {
    /** Deletes Comment entity from storage using id
     *
     * @param newsId - id of entity
     * @throws ServiceException
     */
    void delete(Long newsId) throws ServiceException;

    /**  Creates new Comment entity in storage using instance,
     * sets generated id in instance
     *
     * @param comment - instance for creation
     * @throws ServiceException
     */
    void create(Comment comment) throws ServiceException;

    /** Updates Comment entity in storage using fields from instance
     *
     * @param comment - instance for updating
     * @throws ServiceException
     */
    void update(Comment comment) throws ServiceException;

    /** Finds Comments entities by news id
     *
     * @param id - news id
     * @return List of Comment entities
     * @throws ServiceException
     */
    List<Comment> findByNewsId(Long id) throws ServiceException;

    /** Deletes Comment entities by news id
     *
     * @param newsId - news id
     * @throws ServiceException
     */
    void deleteByNewsId(Long newsId) throws ServiceException;
}
