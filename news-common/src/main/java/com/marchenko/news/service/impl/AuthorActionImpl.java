package com.marchenko.news.service.impl;

import com.marchenko.news.exception.DaoException;
import com.marchenko.news.dao.interfaces.AuthorDao;
import com.marchenko.news.entity.Author;
import com.marchenko.news.exception.ServiceException;
import com.marchenko.news.service.interfaces.AuthorAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Marchenko Vadim on 2/19/2016.
 */
@Component("authorAction")
class AuthorActionImpl implements AuthorAction {
    @Autowired
    private AuthorDao authorDao;

    @Override
    public void createAuthor(Author author) throws ServiceException {
        if (author == null) {
            throw new ServiceException("author is null");
        }
        try {
            Optional<Long> optionalId = authorDao.create(author);
            if (optionalId.isPresent()){
                author.setId(optionalId.get());
            } else {
                throw new ServiceException("Returning id is null with author = " + author);
            }
        } catch (DaoException e) {
            throw new ServiceException("Exception in attachAuthor method", e);
        }
    }

    @Override
    public void deleteAuthor(Long id) throws ServiceException {
        if (id == null) {
            throw new ServiceException("Author's id is null");
        }
        try {
            authorDao.delete(id);
        } catch (DaoException e) {
            throw new ServiceException("Exception in delete author, id = " + id, e);
        }
    }

    @Override
    public void updateAuthor(Author author) throws ServiceException {
        if (author == null){
            throw new ServiceException("Author is null");
        }
        try {
            authorDao.update(author);
        } catch (DaoException e) {
            throw new ServiceException("Exception in update method, author = " + author, e);
        }
    }

    @Override
    public Optional<Author> readAuthor(Long id) throws ServiceException {
        if (id == null){
            throw new ServiceException("Id is null");
        }
        try {
            return authorDao.read(id);
        } catch (DaoException e) {
            throw new ServiceException("Exception in read method, id = " + id);
        }
    }

    @Override
    public List<Author> findAll() throws ServiceException {
        try {
            return authorDao.findAll();
        } catch (DaoException e) {
            throw new ServiceException("Exception in findAll, cause in dao", e);
        }
    }

    @Override
    public List<Author> findNonExpiredAuthors() throws ServiceException {
        try{
            List<Author> authorList = authorDao.findAll();
            authorList = authorList.stream()
                    .filter((author) -> author.getTimeExpired() == null)
                    .collect(Collectors.toList());
            return authorList;
        } catch (DaoException e) {
            throw new ServiceException("Exception in findNonExpiredAuthors", e);
        }
    }

    @Override
    public void makeExpiredAuthor(Long id) throws ServiceException {
        if (id == null){
            throw new ServiceException("Id is null");
        }
        try {
            Optional<Author> authorOptional = authorDao.read(id);
            if (authorOptional.isPresent()){
                Author author = authorOptional.get();
                author.setTimeExpired(LocalDateTime.now());
                authorDao.update(author);
            } else {
                throw new ServiceException("The author with id = " + id + " doesn't exist");
            }
        } catch (DaoException e) {
            throw new ServiceException("Exception from dao layer with id=" + id, e);
        }
    }
}
