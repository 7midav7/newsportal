package com.marchenko.news.service.interfaces;

import com.marchenko.news.entity.Author;
import com.marchenko.news.entity.News;
import com.marchenko.news.entity.SearchCriteria;
import com.marchenko.news.entity.Tag;
import com.marchenko.news.exception.ServiceException;

import java.util.List;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 3/11/2016.
 */

/** Provides operations with News entity
 *  @author Vadzim Marchanka
 */
public interface NewsAction {
    /** Creates News entity in storage using instance
     *
     * @param news - instance for creation
     * @return generated id
     * @throws ServiceException
     */
    Optional<Long> create(News news) throws ServiceException;

    /** Updates News entity in storage using instance
     *
     * @param entity - instance
     * @throws ServiceException
     */
    void update(News entity) throws ServiceException;

    /** Deletes News entity from storage using its id
     *
     * @param id its id
     * @throws ServiceException
     */
    void delete(Long id) throws ServiceException;

    /** Returns News entity from storage by its id
     *
     * @param id - its id
     * @return News entity
     * @throws ServiceException
     */
    Optional<News> read(Long id) throws ServiceException;

    /** Attaches tag to news
     *
     * @param newsId - news id
     * @param tagId - tag id
     * @throws ServiceException
     */
    void attachTag(Long newsId, Long tagId) throws ServiceException;

    /** Attaches author to news
     *
     * @param newsId - news id
     * @param authorId - author id
     * @throws ServiceException
     */
    void attachAuthor(Long newsId, Long authorId) throws ServiceException;

    /** Detaches tag from news
     *
     * @param newsId - news id
     * @param tagId - tag id
     * @throws ServiceException
     */
    void detachTag(Long newsId, Long tagId) throws ServiceException;

    /** Detaches author from news
     *
     * @param newsId - newsId
     * @param authorId - authorId
     * @throws ServiceException
     */
    void detachAuthor(Long newsId, Long authorId) throws ServiceException;

    /** Detaches all tags from news
     *
     * @param newsId - news id
     * @throws ServiceException
     */
    void detachAllTags(Long newsId) throws ServiceException;

    /** Detaches all authors from news
     *
     * @param newsId - news id
     * @throws ServiceException
     */
    void detachAllAuthors(Long newsId) throws ServiceException;

    /** Finds all newses
     *
     * @return List of News entity
     * @throws ServiceException
     */
    List<News> findAll() throws ServiceException;

    /** Finds all attached tags to news
     *
     * @param newsId - news id
     * @return List of Tag entity
     * @throws ServiceException
     */
    List<Tag> findAttachedTags(Long newsId) throws ServiceException;

    /** Finds all attached authors to news
     *
     * @param newsId - news id
     * @return List of Author entity
     * @throws ServiceException
     */
    List<Author> findAttachedAuthors(Long newsId) throws ServiceException;

    /** Finds all newses, sorts them by count of comments, returns result
     * in range[firstIndex, lastIndex]
     *
     * @param firstIndex - firstIndex
     * @param lastIndex - secondIndex
     * @return List of Author entity
     * @throws ServiceException
     */
    List<News> takeMostCommentedNews(Long firstIndex, Long lastIndex) throws ServiceException;

    /** Finds newses according search criteria, returns result
     * in range[firstIndex, lastIndex]
     *
     * @param searchCriteria - search criteria
     * @param firstIndex - firstIndex
     * @param lastIndex - secondIndex
     * @return List of News entity
     * @throws ServiceException
     */
    List<News> findBySearchCriteria(SearchCriteria searchCriteria,
                                    Long firstIndex, Long lastIndex) throws ServiceException;

    /** Returns the count of newses according search criteria
     *
     * @param searchCriteria - search criteria
     * @return the count
     * @throws ServiceException
     */
    Long countNewsesBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;

    /** Returns the count of all newses
     *
     * @return the count
     * @throws ServiceException
     */
    Long countNewses() throws ServiceException;
}
