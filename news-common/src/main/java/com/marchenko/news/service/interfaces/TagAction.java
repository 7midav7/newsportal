package com.marchenko.news.service.interfaces;

import com.marchenko.news.entity.Tag;
import com.marchenko.news.exception.ServiceException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 2/19/2016.
 */

/** Provides operations with Tag entity
 *  @author Vadzim Marchanka
 */
public interface TagAction {
    /** Creates Tag entity in storage by instance, sets generated id in instance
     *
     * @param tag - instance
     * @throws ServiceException
     */
    void createTag(Tag tag) throws ServiceException;

    /** Deletes Tag entity from storage by its id
     *
     * @param tagId - its id
     * @throws ServiceException
     */
    void deleteTag(Long tagId) throws ServiceException;

    /** Updates Tag entity in storage by instance
     *
     * @param tag - instance
     * @throws ServiceException
     */
    void updateTag(Tag tag) throws ServiceException;

    /** Returns Tag entity from storage according to its id
     *
     * @param id - its id
     * @return Optional of Tag entity
     * @throws ServiceException
     */
    Optional<Tag> readTag(Long id) throws ServiceException;

    /** Finds all Tag entity from storage
     *
     * @return - List of Tag entities
     * @throws ServiceException
     */
    List<Tag> findAll() throws ServiceException;

    /** Reads all Tag entities according List of their id
     *
     * @param idSequence - their id
     * @return - List of Tag entities
     * @throws ServiceException
     */
    default List<Tag> readSequence(List<Long> idSequence) throws ServiceException {
        if (idSequence == null){
            throw new ServiceException("Parameter (idSecuence) is null ");
        }
        ArrayList<Tag> tags = new ArrayList<>();
        for (Long id : idSequence){
            Optional<Tag> optional = readTag(id);
            if (optional.isPresent()) {
                tags.add(optional.get());
            }
        }
        return tags;
    }
}
