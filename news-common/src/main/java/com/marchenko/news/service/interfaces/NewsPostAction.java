package com.marchenko.news.service.interfaces;

import com.marchenko.news.entity.News;
import com.marchenko.news.exception.ServiceException;
import com.marchenko.news.entity.NewsPost;

import java.util.List;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 2/17/2016.
 */

/** Provides operations with NewsPost entity
 *  @author Vadzim Marchanka
 */
public interface NewsPostAction {
    /** Creates News entity with attached Author and list of Tag entities
     * in storage according NewsPost instance
     *
     * @param newsPost - instance
     * @return generated id
     * @throws ServiceException
     */
    Long createNewsPost(NewsPost newsPost) throws ServiceException;

    /** Updates News entity in storage, attaches or detaches tags and authors
     *
     * @param newsPost
     * @throws ServiceException
     */
    void editNewsPostContent(NewsPost newsPost) throws ServiceException;

    /** Deletes News entity from storage, detaches all tags and author
     *
     * @param newsId - news id
     * @throws ServiceException
     */
    void deleteNewsMessage(Long newsId) throws ServiceException;

    /** Reads News entity from storage with attached tags, comments and author
     *
     * @param newsId - news id
     * @return NewsPost entity
     * @throws ServiceException
     */
    Optional<NewsPost> readNews(Long newsId) throws ServiceException;

    /** Converts List of News entity to List of NewsPost entities by finding
     * attached comments, tags and author
     *
     * @param newses
     * @return List of NewsPost entity
     * @throws ServiceException
     */
    List<NewsPost> convert(List<News> newses) throws ServiceException;
}
