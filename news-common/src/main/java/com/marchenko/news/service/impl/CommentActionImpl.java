package com.marchenko.news.service.impl;

import com.marchenko.news.exception.DaoException;
import com.marchenko.news.dao.interfaces.CommentDao;
import com.marchenko.news.entity.Comment;
import com.marchenko.news.exception.ServiceException;
import com.marchenko.news.service.interfaces.CommentAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 2/19/2016.
 */
@Component("commentAction")
class CommentActionImpl implements CommentAction {
    @Autowired
    private CommentDao commentDao;

    @Override
    public void delete(Long id) throws ServiceException {
        if (id == null) {
            throw new ServiceException("Author's id is null");
        }
        try {
            commentDao.delete(id);
        } catch (DaoException e) {
            throw new ServiceException("Error in delete method ", e);
        }
    }

    @Override
    public void create(Comment comment) throws ServiceException {
        if (comment == null) {
            throw new ServiceException("Comment id is null");
        }
        try {
            comment.setCreationDate(LocalDateTime.now());
            Optional<Long> optionalId = commentDao.create(comment);
            if (optionalId.isPresent()){
                comment.setId(optionalId.get());
            } else {
                throw new ServiceException("Dao didn't create an entity = " + comment);
            }
        } catch (DaoException e) {
            throw new ServiceException("Error in create method ", e);
        }
    }

    public void update(Comment comment) throws ServiceException {
        if (comment == null) {
            throw new ServiceException("Comment is null");
        }
        try {
            commentDao.update(comment);
        } catch (DaoException e) {
            throw new ServiceException("", e);
        }
    }

    public List<Comment> findByNewsId(Long id) throws ServiceException {
        if (id == null) {
            throw new ServiceException("Comment id is null");
        }
        try {
            return commentDao.findByNewsId(id);
        } catch (DaoException e) {
            throw new ServiceException("Error in findComment with id:" + id, e);
        }
    }

    public void deleteByNewsId(Long newsId) throws ServiceException {
        if (newsId == null) {
            throw new ServiceException("Id of news is null");
        }
        try {
            commentDao.removeByNewsId(newsId);
        } catch (DaoException e) {
            throw new ServiceException("Error in deleteCommentsByNewsId with id:" + newsId, e);
        }
    }
}
