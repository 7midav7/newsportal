package com.marchenko.news.entity;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

public class Comment extends Entity {
	private static final long serialVersionUID = 2366413571234473145L;
	private Long id;
	private Long newsId;
	@NotNull(message = "there is no string")
	@Size(min=1, max = 100, message = "Size is more than 100 or string is empty")
	private String text;
	private LocalDateTime creationDate;

	public Comment(String text) {
		super();
		this.text = text;
		this.creationDate = LocalDateTime.now();
	}

	public Comment(){

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Comment comment = (Comment) o;

		if (id != null ? !id.equals(comment.id) : comment.id != null) return false;
		if (newsId != null ? !newsId.equals(comment.newsId) : comment.newsId != null) return false;
		if (text != null ? !text.equals(comment.text) : comment.text != null) return false;
		return creationDate != null ? creationDate.equals(comment.creationDate) : comment.creationDate == null;

	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (newsId != null ? newsId.hashCode() : 0);
		result = 31 * result + (text != null ? text.hashCode() : 0);
		result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Comment{" +
				"id=" + id +
				", newsId=" + newsId +
				", text='" + text + '\'' +
				", creationDate=" + creationDate +
				'}';
	}
}
