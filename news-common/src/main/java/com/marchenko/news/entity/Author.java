package com.marchenko.news.entity;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

public class Author extends Entity {
	private static final long serialVersionUID = -4620602838539779799L;
	private Long id;
	@NotNull(message = "there is no string")
	@Size(min=1, max = 30, message = "The size is more than 30 or string is empty")
	private String name;
	private LocalDateTime timeExpired;

	public Author(String name) {
		super();
		this.name = name;
	}

	public Author(){

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getTimeExpired() {
		return timeExpired;
	}

	public void setTimeExpired(LocalDateTime timeExpired) {
		this.timeExpired = timeExpired;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Author author = (Author) o;

		if (id != null ? !id.equals(author.id) : author.id != null) return false;
		if (name != null ? !name.equals(author.name) : author.name != null) return false;
		return timeExpired != null ? timeExpired.equals(author.timeExpired) : author.timeExpired == null;

	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (timeExpired != null ? timeExpired.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Author{" +
				"id=" + id +
				", name='" + name + '\'' +
				", timeExpired=" + timeExpired +
				'}';
	}
}
