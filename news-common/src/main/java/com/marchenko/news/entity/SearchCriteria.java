package com.marchenko.news.entity;

import java.util.List;

/**
 * Created by Marchenko Vadim on 2/19/2016.
 */
public class SearchCriteria {
    private Long authorId;
    private List<Long> tagIdList;

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public List<Long> getTagIdList() {
        return tagIdList;
    }

    public void setTagIdList(List<Long> tagIdList) {
        this.tagIdList = tagIdList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchCriteria that = (SearchCriteria) o;

        if (authorId != null ? !authorId.equals(that.authorId) : that.authorId != null) return false;
        return tagIdList != null ? tagIdList.equals(that.tagIdList) : that.tagIdList == null;

    }

    @Override
    public int hashCode() {
        int result = authorId != null ? authorId.hashCode() : 0;
        result = 31 * result + (tagIdList != null ? tagIdList.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SearchCriteria{" +
                "authorId=" + authorId +
                ", tagIdList=" + tagIdList +
                '}';
    }
}
