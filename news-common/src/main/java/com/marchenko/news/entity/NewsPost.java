package com.marchenko.news.entity;

import java.io.Serializable;
import java.util.List;

public class NewsPost implements Serializable {
	private static final long serialVersionUID = 1881824680867881453L;
	private Author author;
	private News news;
	private List<Tag> tags;
	private List<Comment> comments;

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		NewsPost that = (NewsPost) o;

		if (author != null ? !author.equals(that.author) : that.author != null) return false;
		if (news != null ? !news.equals(that.news) : that.news != null) return false;
		return tags != null ? tags.equals(that.tags) : that.tags == null;

	}

	@Override
	public int hashCode() {
		int result = author != null ? author.hashCode() : 0;
		result = 31 * result + (news != null ? news.hashCode() : 0);
		result = 31 * result + (tags != null ? tags.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "NewsPost{" +
				"author=" + author +
				", news=" + news +
				", tags=" + tags +
				", comments=" + comments +
				'}';
	}
}
