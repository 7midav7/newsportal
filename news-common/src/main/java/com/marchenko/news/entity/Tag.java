package com.marchenko.news.entity;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Tag extends Entity {
	private static final long serialVersionUID = 4823009178099870756L;
	private Long id;
	
	@NotNull(message = "there is no string")
	@Size(min=1, max = 30, message = "Size is more then 30 or string is empty")
	private String tagName;

	public Tag(String tagName) {
		super();
		this.tagName = tagName;
	}

	public Tag(){

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Tag tag = (Tag) o;

		if (id != null ? !id.equals(tag.id) : tag.id != null) return false;
		return tagName != null ? tagName.equals(tag.tagName) : tag.tagName == null;

	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (tagName != null ? tagName.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Tag{" +
				"id=" + id +
				", tagName='" + tagName + '\'' +
				'}';
	}
}
