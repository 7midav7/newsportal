package com.marchenko.news.dao.impl;

import com.marchenko.news.exception.DaoException;
import com.marchenko.news.dao.interfaces.AuthorDao;
import com.marchenko.news.entity.Author;
import org.apache.commons.dbcp.BasicDataSource;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.oracle.Oracle10DataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 2/21/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/test/resources/test_spring.xml"})
public class AuthorDaoImplTest {
    private static BasicDataSource dataSource;
    @Autowired
    private AuthorDao authorDao;

    static{
        ConfigurableApplicationContext ctx = new ClassPathXmlApplicationContext("file:src/test/resources/test_spring.xml");
        dataSource = (BasicDataSource) ctx.getBean("dataSource");
    }

    @BeforeClass
    public static void init() throws Exception{
        DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSet());
    }

    @AfterClass
    public static void after() throws Exception{
        DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSet());
    }

    public static IDatabaseConnection getConnection() throws Exception {
        Connection con = dataSource.getConnection();
        DatabaseMetaData databaseMetaData = con.getMetaData();
        IDatabaseConnection connection = new DatabaseConnection(con,databaseMetaData.getUserName().toUpperCase());
        DatabaseConfig config = connection.getConfig();
        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new Oracle10DataTypeFactory());
        return connection;
    }

    public static IDataSet getDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(
                new FileInputStream("src/test/resources/com/marchenko/news/dao/impl/AuthorDaoImplTest.xml"));
    }

    @Test
    public void readTest() throws DaoException {
        caseWithoutTimeExpired();
        casePresentAuthor();
        caseNonExistingAuthor();
    }

    @Test
    public void createTest() throws DaoException{
        caseRussianLocale();
        caseEnglishLocale();
    }

    @Test
    public void deleteTest() throws DaoException{
        caseDeleteExistingAuthor();
        caseDeleteNonExistingAuthor();
    }

    @Test
    public void updateTest() throws DaoException{
        caseUpdateExistingAuthor();
        caseUpdateNonExistingAuthor();
    }

    private void caseWithoutTimeExpired() throws DaoException {
        Optional<Author> optionalWithoutDate = authorDao.read(1L);
        Author author = optionalWithoutDate.get();
        Assert.assertEquals("igor", author.getName());
        Assert.assertEquals(Long.valueOf(1), author.getId());
        Assert.assertNull(author.getTimeExpired());
    }

    private void casePresentAuthor() throws DaoException {
        Optional<Author> optionalWithDate = authorDao.read(2L);
        Author author = optionalWithDate.get();
        Assert.assertEquals("artem", author.getName());
        Assert.assertEquals(Long.valueOf(2), author.getId());
        Assert.assertEquals(LocalDateTime.of(2014,4,6,9,1,10), author.getTimeExpired());
    }

    private void caseNonExistingAuthor() throws DaoException {
        Optional<Author> emptyOptional = authorDao.read(100500L);
        Assert.assertEquals(emptyOptional.isPresent(), false);
    }

    private void caseEnglishLocale() throws DaoException {
        Author author = new Author("dima");
        author.setTimeExpired(LocalDateTime.of(2014,4,6,9,1,10));
        long generatedId = authorDao.create(author).get();
        author.setId(generatedId);
        Optional<Author> optional = authorDao.read(generatedId);
        Author databaseAuthor = optional.get();
        Assert.assertEquals(author, databaseAuthor);
    }

    private void caseRussianLocale() throws DaoException {
        Author author = new Author("Дима");
        author.setTimeExpired(LocalDateTime.of(2014,4,6,9,1,10));
        long generatedId = authorDao.create(author).get();
        author.setId(generatedId);
        Optional<Author> optional = authorDao.read(generatedId);
        Author databaseAuthor = optional.get();
        Assert.assertEquals(author, databaseAuthor);
    }

    private void caseDeleteExistingAuthor() throws DaoException {
        Optional<Author> optional = authorDao.read(3L);
        Assert.assertEquals(optional.isPresent(), true);
        authorDao.delete(3L);
        optional = authorDao.read(3L);
        Assert.assertEquals(optional.isPresent(), false);
    }

    private void caseDeleteNonExistingAuthor() throws DaoException {
        authorDao.delete(100500L);
    }

    private void caseUpdateExistingAuthor() throws DaoException{
        Optional<Author> optional = authorDao.read(4L);
        Author author = optional.get();
        author.setName("update");
        author.setTimeExpired(LocalDateTime.of(2015,4,6,9,1,10));
        authorDao.update(author);
        optional = authorDao.read(4L);
        Author databaseAuthor = optional.get();
        Assert.assertEquals(author, databaseAuthor);
    }

    private void caseUpdateNonExistingAuthor() throws DaoException{
        Author author = new Author("nonExisting");
        author.setId(100500L);
        author.setTimeExpired(LocalDateTime.of(2015,4,6,9,1,10));
        try {
            authorDao.update(author);
        } catch (DaoException e){
            return;
        }
        Assert.fail("Exceptional situation without DaoException(there is no id 100500)");
    }
}
