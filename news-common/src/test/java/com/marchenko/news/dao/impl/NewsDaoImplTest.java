package com.marchenko.news.dao.impl;

import com.marchenko.news.exception.DaoException;
import com.marchenko.news.dao.interfaces.AuthorDao;
import com.marchenko.news.dao.interfaces.CommentDao;
import com.marchenko.news.dao.interfaces.NewsDao;
import com.marchenko.news.dao.interfaces.TagDao;
import com.marchenko.news.entity.*;
import org.apache.commons.dbcp.BasicDataSource;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.oracle.Oracle10DataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 2/23/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/test/resources/test_spring.xml"})
public class NewsDaoImplTest {
    private static BasicDataSource dataSource;
    @Autowired
    @Qualifier("newsDao")
    private NewsDao newsDao;
    @Autowired
    @Qualifier("commentDao")
    private CommentDao commentDao;
    @Autowired
    @Qualifier("tagDao")
    private TagDao tagDao;
    @Autowired
    @Qualifier("authorDao")
    private AuthorDao authorDao;

    static {
        ConfigurableApplicationContext ctx = new ClassPathXmlApplicationContext("file:src/test/resources/test_spring.xml");
        dataSource = (BasicDataSource) ctx.getBean("dataSource");
    }

    @BeforeClass
    public static void init() throws Exception {
        DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSet());
    }

    @AfterClass
    public static void after() throws Exception {
        DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSet());
    }

    public static IDatabaseConnection getConnection() throws Exception {
        Connection con = dataSource.getConnection();
        DatabaseMetaData databaseMetaData = con.getMetaData();
        IDatabaseConnection connection = new DatabaseConnection(con, databaseMetaData.getUserName().toUpperCase());
        DatabaseConfig config = connection.getConfig();
        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new Oracle10DataTypeFactory());
        return connection;
    }

    public static IDataSet getDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(
                new FileInputStream("src/test/resources/com/marchenko/news/dao/impl/NewsDaoImplTest.xml"));
    }

    @Test
    public void create() throws DaoException {
        News news = new News("title", "fullText");
        news.setCreationDate(LocalDateTime.of(2014, 4, 6, 9, 1, 10));
        news.setModificationDate(LocalDateTime.of(2014, 4, 6, 0, 0, 0));
        news.setShortText("shortText");
        long newsId = newsDao.create(news).get();
        news.setId(newsId);
        Optional<News> optional = newsDao.read(newsId);
        News readedNews = optional.get();
        Assert.assertEquals(readedNews, news);
    }

    @Test
    public void read() throws DaoException {
        News news = new News("title70", "full_text");
        news.setCreationDate(LocalDateTime.of(2014, 4, 6, 9, 1, 10));
        news.setModificationDate(LocalDateTime.of(2014, 4, 6, 0, 0, 0));
        news.setShortText("short_text70");
        news.setId(70L);
        Optional<News> optional = newsDao.read(70L);
        News readedNews = optional.get();
        Assert.assertEquals(readedNews, news);
    }

    @Test
    public void delete() throws DaoException {
        Optional<News> optional = newsDao.read(71L);
        Assert.assertEquals(optional.isPresent(), true);
        newsDao.delete(71L);
        optional = newsDao.read(71L);
        Assert.assertEquals(optional.isPresent(), false);
    }

    @Test
    public void update() throws DaoException {
        News news = new News("updated", "updated");
        news.setId(72L);
        news.setCreationDate(LocalDateTime.of(2003, 4, 6, 9, 1, 10));
        news.setModificationDate(LocalDateTime.of(2003, 4, 6, 0, 0, 0));
        news.setShortText("updated");
        newsDao.update(news);
        Optional<News> optional = newsDao.read(72L);
        News readedNews = optional.get();
        Assert.assertEquals(news, readedNews);
    }

    @Test
    public void attachTag() throws DaoException {
        newsDao.attachTag(74L, 30L);
        List<Tag> list = newsDao.findAttachedTags(74L);
        Assert.assertEquals(list.size(), 1);
        Tag tag = tagDao.read(30L).get();
        Assert.assertEquals(tag, list.get(0));
    }

    @Test
    public void detachTag() throws DaoException {
        List<Tag> list = newsDao.findAttachedTags(75L);
        Assert.assertEquals(list.size(), 1);
        newsDao.detachTag(75L, 31L);
        list = newsDao.findAttachedTags(75L);
        Assert.assertEquals(list.size(), 0);
    }

    @Test
    public void detachAllTags() throws DaoException {
        List<Tag> list = newsDao.findAttachedTags(76L);
        Assert.assertEquals(list.size(), 2);
        newsDao.detachAllTags(76L);
        list = newsDao.findAttachedTags(76L);
        Assert.assertEquals(list.size(), 0);
    }

    @Test
    public void findAllAttachedTags() throws DaoException {
        List<Tag> tags = newsDao.findAttachedTags(73L);
        tags.sort((a, b) -> Long.compare(a.getId(), b.getId()));
        Tag tag1 = new Tag("tag1");
        tag1.setId(20L);
        Tag tag2 = new Tag("tag2");
        tag2.setId(21L);
        Assert.assertEquals(tags.get(0), tag1);
        Assert.assertEquals(tags.get(1), tag2);
    }

    @Test
    public void findAllAttachedAuthors() throws DaoException {
        List<Author> authors = newsDao.findAttachedAuthors(77L);
        Assert.assertEquals(authors.size(), 2);
        authors.sort((a, b) -> Long.compare(a.getId(), b.getId()));
        Author author1 = authorDao.read(10L).get();
        Author author2 = authorDao.read(11L).get();
        Assert.assertEquals(authors.get(0), author1);
        Assert.assertEquals(authors.get(1), author2);
    }

    @Test
    public void attachAuthor() throws DaoException {
        List<Author> authors = newsDao.findAttachedAuthors(78L);
        Assert.assertEquals(authors.size(), 0);
        newsDao.attachAuthor(78L, 12L);
        authors = newsDao.findAttachedAuthors(78L);
        Assert.assertEquals(authors.size(), 1);
        Author author = authorDao.read(12L).get();
        Assert.assertEquals(author, authors.get(0));
    }

    @Test
    public void detachAllAuthors() throws DaoException {
        List<Author> authors = newsDao.findAttachedAuthors(79L);
        Assert.assertEquals(authors.size(), 3);
        newsDao.detachAllAuthors(79L);
        authors = newsDao.findAttachedAuthors(79L);
        Assert.assertEquals(authors.size(), 0);
    }

    @Test
    public void detachAuthor() throws DaoException {
        List<Author> authors = newsDao.findAttachedAuthors(80L);
        Assert.assertEquals(authors.size(), 1);
        newsDao.detachAuthor(80L, 16L);
        authors = newsDao.findAttachedAuthors(80L);
        Assert.assertEquals(authors.size(), 0);
    }

    @Test
    public void findAll() throws DaoException {
        List<News> newses = newsDao.findAll();
        Assert.assertEquals(newses.size(), newsDao.countNewses());
    }

    @Test
    public void takeMostCommentedNewsTest() throws DaoException {
        List<News> newses = newsDao.takeMostCommentedNews(0L, 100L);
        int lastCommentCount = 200005000;
        for (News news : newses) {
            long newsId = news.getId();
            News readedNews = newsDao.read(newsId).get();
            Assert.assertEquals(news, readedNews);
            List<Comment> comments = commentDao.findByNewsId(newsId);
            Assert.assertTrue(comments.size() <= lastCommentCount);
            lastCommentCount = comments.size();
        }
    }

    @Test
    public void countNewsesTest() throws DaoException {
        List<News> newses = newsDao.findAll();
        Assert.assertEquals(newses.size(), newsDao.countNewses());
    }

    @Test
    public void findBySearchCriteriaTest() throws DaoException {
        //TODO: change on actual
        /*News news82 = newsDao.read(82L).get();
        News news83 = newsDao.read(83L).get();
        News news84 = newsDao.read(84L).get();
        Tag tag50 = tagDao.read(50L).get();
        Tag tag52 = tagDao.read(52L).get();
        Author author30 = authorDao.read(30L).get();
        Author author31 = authorDao.read(31L).get();
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setAuthor(author30);
        List<News> newses = newsDao.findBySearchCriteria(searchCriteria);
        newses.sort((a, b) -> Long.compare(a.getId(), b.getId()));
        Assert.assertEquals(news82, newses.get(0));
        Assert.assertEquals(news84, newses.get(1));
        Assert.assertEquals(newses.size(), 2);
        searchCriteria.setAuthor(author31);
        newses = newsDao.findBySearchCriteria(searchCriteria);
        Assert.assertEquals(newses.size(), 1);
        Assert.assertEquals(newses.get(0), news83);
        searchCriteria.setAuthor(null);
        List<Tag> tags = new ArrayList<>();
        tags.add(tag50);
        searchCriteria.setTags(tags);
        newses = newsDao.findBySearchCriteria(searchCriteria);
        Assert.assertEquals(newses.size(), 2);
        newses.sort((a, b) -> Long.compare(a.getId(), b.getId()));
        Assert.assertEquals(newses.get(0), news82);
        Assert.assertEquals(newses.get(1), news83);
        tags.add(tag52);
        newses = newsDao.findBySearchCriteria(searchCriteria);
        Assert.assertEquals(newses.size(), 3);
        newses.sort((a, b) -> Long.compare(a.getId(), b.getId()));
        Assert.assertEquals(newses.get(0), news82);
        Assert.assertEquals(newses.get(1), news83);
        Assert.assertEquals(newses.get(2), news84);
        searchCriteria.setAuthor(author31);
        newses = newsDao.findBySearchCriteria(searchCriteria);
        Assert.assertEquals(newses.size(), 1);
        Assert.assertEquals(newses.get(0), news83);*/
    }
}
