package com.marchenko.news.dao.impl;

import com.marchenko.news.exception.DaoException;
import com.marchenko.news.dao.interfaces.TagDao;
import com.marchenko.news.entity.Tag;
import org.apache.commons.dbcp.BasicDataSource;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.oracle.Oracle10DataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 2/22/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/test/resources/test_spring.xml"})
public class TagDaoImplTest {
    private static BasicDataSource dataSource;
    @Autowired
    private TagDao tagDao;

    static {
        ConfigurableApplicationContext ctx = new ClassPathXmlApplicationContext("file:src/test/resources/test_spring.xml");
        dataSource = (BasicDataSource) ctx.getBean("dataSource");
    }


    @BeforeClass
    public static void init() throws Exception {
        DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSet());
    }

    @AfterClass
    public static void after() throws Exception {
        DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSet());
    }

    public static IDatabaseConnection getConnection() throws Exception {
        Connection con = dataSource.getConnection();
        DatabaseMetaData databaseMetaData = con.getMetaData();
        IDatabaseConnection connection = new DatabaseConnection(con, databaseMetaData.getUserName().toUpperCase());
        DatabaseConfig config = connection.getConfig();
        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new Oracle10DataTypeFactory());
        return connection;
    }

    public static IDataSet getDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(
                new FileInputStream("src/test/resources/com/marchenko/news/dao/impl/TagDaoImplTest.xml"));
    }

    @Test
    public void readTest() throws DaoException {
        Optional<Tag> optional = tagDao.read(100500L);
        Assert.assertEquals(optional.isPresent(), false);
        optional = tagDao.read(1L);
        Tag tag = optional.get();
        Assert.assertEquals(tag.getId(), Long.valueOf(1L));
        Assert.assertEquals(tag.getTagName(), "tag");
    }

    @Test
    public void updateTest() throws DaoException {
        Optional<Tag> optional = tagDao.read(3L);
        Tag tag = optional.get();
        tag.setTagName("updated");
        tagDao.update(tag);
        optional = tagDao.read(3L);
        tag = optional.get();
        Assert.assertEquals(tag.getTagName(), "updated");
    }

    @Test
    public void deleteTest() throws DaoException {
        Optional<Tag> optional = tagDao.read(2L);
        Assert.assertEquals(optional.isPresent(), true);
        tagDao.delete(2L);
        optional = tagDao.read(2L);
        Assert.assertEquals(optional.isPresent(), false);
    }

    @Test
    public void createTest() throws DaoException {
        Tag tag = new Tag("createdTag");
        long tagId = tagDao.create(tag).get();
        Optional<Tag> optional = tagDao.read(tagId);
        tag = optional.get();
        Assert.assertEquals(tag.getId(), Long.valueOf(tagId));
        Assert.assertEquals(tag.getTagName(), "createdTag");
    }
}
