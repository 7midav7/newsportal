package com.marchenko.news.dao.impl;

/**
 * Created by Marchenko Vadim on 2/22/2016.
 */
import com.marchenko.news.exception.DaoException;
import com.marchenko.news.dao.interfaces.CommentDao;
import com.marchenko.news.entity.Comment;
import org.apache.commons.dbcp.BasicDataSource;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.oracle.Oracle10DataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/test/resources/test_spring.xml"})
public class CommentDaoImplTest {
    private static BasicDataSource dataSource;
    static{
        ConfigurableApplicationContext ctx =  new ClassPathXmlApplicationContext("file:src/test/resources/test_spring.xml");
        dataSource = (BasicDataSource) ctx.getBean("dataSource");
    }
    @Autowired
    @Qualifier("commentDao")
    private CommentDao commentDao;

    @BeforeClass
    public static void init() throws Exception{
        DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSet());
    }

    @AfterClass
    public static void after() throws Exception{
        DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSet());
    }

    public static IDatabaseConnection getConnection() throws Exception {
        Connection con = dataSource.getConnection();
        DatabaseMetaData databaseMetaData = con.getMetaData();
        IDatabaseConnection connection = new DatabaseConnection(con,databaseMetaData.getUserName().toUpperCase());
        DatabaseConfig config = connection.getConfig();
        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new Oracle10DataTypeFactory());
        return connection;
    }

    public static IDataSet getDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(
                new FileInputStream("src/test/resources/com/marchenko/news/dao/impl/CommentDaoImplTest.xml"));
    }

    @Test
    public void readTest() throws DaoException {
        Optional<Comment> optional = commentDao.read(1L);
        Comment comment = optional.get();
        Assert.assertEquals(comment.getId(), Long.valueOf(1));
        Assert.assertEquals(comment.getNewsId(), Long.valueOf(50));
        Assert.assertEquals(comment.getText(), "text");
        Assert.assertEquals(comment.getCreationDate(), LocalDateTime.of(2014,4,6,9,1,10));
    }

    @Test
    public void updateTest() throws DaoException {
        Optional<Comment> optional = commentDao.read(2L);
        Comment comment = optional.get();
        comment.setText("newText");
        commentDao.update(comment);
        optional = commentDao.read(2L);
        Comment updatedComment = optional.get();
        Assert.assertEquals(updatedComment, comment);
    }

    @Test
    public void deleteTest() throws DaoException {
        Optional<Comment> optional = commentDao.read(3L);
        Assert.assertEquals(optional.isPresent(), true);
        commentDao.delete(3L);
        optional = commentDao.read(3L);
        Assert.assertEquals(optional.isPresent(), false);
    }

    @Test
    public void createTest() throws DaoException {
        Comment comment = new Comment("text3");
        comment.setNewsId(50L);
        long commentId = commentDao.create(comment).get();
        comment.setId(commentId);
        Optional<Comment> optional = commentDao.read(commentId);
        Comment createdComment = optional.get();
        Assert.assertEquals(comment, createdComment);
    }

    @Test
    public void removeByNewsIdTest() throws DaoException{
        Optional<Comment> optional1 = commentDao.read(8L);
        Optional<Comment> optional2 = commentDao.read(9L);
        Optional<Comment> optional3 = commentDao.read(10L);
        Assert.assertEquals(optional1.isPresent(), true);
        Assert.assertEquals(optional2.isPresent(), true);
        Assert.assertEquals(optional3.isPresent(), true);
        commentDao.removeByNewsId(51L);
        optional1 = commentDao.read(8L);
        optional2 = commentDao.read(9L);
        optional3 = commentDao.read(10L);
        Assert.assertEquals(optional1.isPresent(), false);
        Assert.assertEquals(optional2.isPresent(), false);
        Assert.assertEquals(optional3.isPresent(), false);
    }

    @Test
    public void findByNewsIdTest() throws DaoException {
        List<Comment> findedComments = commentDao.findByNewsId(52L);
        findedComments.sort((a, b) -> Long.compare(a.getId(), b.getId()));
        Comment comment1 = new Comment("text5");
        comment1.setNewsId(52L);
        comment1.setId(11L);
        comment1.setCreationDate(LocalDateTime.of(2015,4,6,9,1,10));
        Comment comment2 = new Comment("text5");
        comment2.setNewsId(52L);
        comment2.setId(12L);
        comment2.setCreationDate(LocalDateTime.of(2015,4,6,9,1,10));
        Comment comment3 = new Comment("text5");
        comment3.setNewsId(52L);
        comment3.setId(13L);
        comment3.setCreationDate(LocalDateTime.of(2015,4,6,9,1,10));
        Assert.assertEquals(findedComments.get(0), comment1);
        Assert.assertEquals(findedComments.get(1), comment2);
        Assert.assertEquals(findedComments.get(2), comment3);
    }
}
