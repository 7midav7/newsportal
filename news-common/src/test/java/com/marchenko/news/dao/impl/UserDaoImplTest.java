package com.marchenko.news.dao.impl;

import com.marchenko.news.dao.interfaces.UserDao;
import com.marchenko.news.entity.User;
import com.marchenko.news.exception.DaoException;
import org.apache.commons.dbcp.BasicDataSource;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.oracle.Oracle10DataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.jws.soap.SOAPBinding;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 4/6/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/test/resources/test_spring.xml"})
public class UserDaoImplTest {
    private static final String EN_NAME_VALUE = "name";
    private static final String EN_LOGIN_VALUE = "login";
    private static final String EN_PASSWORD_VALUE = "password";
    private static final String RU_NAME_VALUE = "имя";
    private static final String RU_LOGIN_VALUE = "логин";
    private static final String RU_PASSWORD_VALUE = "пароль";

    private static BasicDataSource dataSource;
    @Autowired
    private UserDao userDao;

    static{
        ConfigurableApplicationContext ctx = new ClassPathXmlApplicationContext("file:src/test/resources/test_spring.xml");
        dataSource = (BasicDataSource) ctx.getBean("dataSource");
    }

    @BeforeClass
    public static void init() throws Exception{
        DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSet());
    }

    @AfterClass
    public static void after() throws Exception{
        DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSet());
    }

    public static IDatabaseConnection getConnection() throws Exception {
        Connection con = dataSource.getConnection();
        DatabaseMetaData databaseMetaData = con.getMetaData();
        IDatabaseConnection connection = new DatabaseConnection(con,databaseMetaData.getUserName().toUpperCase());
        DatabaseConfig config = connection.getConfig();
        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new Oracle10DataTypeFactory());
        return connection;
    }

    public static IDataSet getDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(
                new FileInputStream("src/test/resources/com/marchenko/news/dao/impl/UserDaoImplTest.xml"));
    }

    @Test
    public void readTest() throws DaoException {
        ReadingCasePresentUser();
        ReadingCaseNonExistingUser();
    }

    @Test
    public void createTest() throws DaoException{
        CreatingCaseEnglishLocale();
        CreatingCaseRussianLocale();
    }

    @Test
    public void deleteTest() throws DaoException{
        DeletingCaseExistingUser();
        DeletingCaseNonExistingUser();
    }

    @Test
    public void updateTest() throws DaoException{
        UpdatingCaseExistingUser();
        UpdatingCaseNonExistingUser();
    }

    private void ReadingCasePresentUser() throws DaoException {
        User user = new User();
        user.setId(102L);
        user.setName(EN_NAME_VALUE + "102");
        user.setLogin(EN_LOGIN_VALUE + "102");
        user.setPassword(EN_PASSWORD_VALUE + "102");
        User actualUser = userDao.read(102L).get();
        Assert.assertEquals(user, actualUser);
    }

    private void ReadingCaseNonExistingUser() throws DaoException {
        Optional<User> optional = userDao.read(100500L);
        if (optional.isPresent()){
            Assert.fail("Found non existing user");
        }
    }

    private void CreatingCaseEnglishLocale() throws DaoException {
        User user = new User();
        user.setName(EN_NAME_VALUE + "1");
        user.setLogin(EN_LOGIN_VALUE + "1");
        user.setPassword(EN_PASSWORD_VALUE + "1");
        Long id = userDao.create(user).get();
        user.setId(id);
        User actualUser = userDao.read(id).get();
        Assert.assertEquals(user, actualUser);
    }

    private void CreatingCaseRussianLocale() throws DaoException {
        User user = new User();
        user.setName(RU_NAME_VALUE + "2");
        user.setLogin(RU_LOGIN_VALUE + "2");
        user.setPassword(RU_PASSWORD_VALUE + "2");
        Long id = userDao.create(user).get();
        user.setId(id);
        User actualUser = userDao.read(id).get();
        Assert.assertEquals(user, actualUser);
    }

    private void DeletingCaseExistingUser() throws DaoException {
        User user = userDao.read(104L).get();
        userDao.delete(104L);
        Optional<User> optional = userDao.read(104L);
        Assert.assertFalse(optional.isPresent());
    }

    private void DeletingCaseNonExistingUser() throws DaoException {
        userDao.delete(100500L);
    }

    private void UpdatingCaseExistingUser() throws DaoException{
        User user = userDao.read(103L).get();
        user.setName("update");
        user.setLogin("update");
        user.setPassword("update");
        userDao.update(user);
        User actual = userDao.read(103L).get();
        Assert.assertEquals(user, actual);
    }

    private void UpdatingCaseNonExistingUser() throws DaoException{
        User user = new User();
        user.setId(100500L);
        user.setName("name");
        user.setLogin("login");
        user.setPassword("password");
        try{
            userDao.update(user);
        } catch (DaoException e){
            return;
        }
        Assert.fail("Must be exception, because user with id 100500 doesn't exist");
    }
}
