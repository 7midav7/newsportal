package com.marchenko.news.service.impl;

import com.marchenko.news.exception.DaoException;
import com.marchenko.news.entity.*;
import com.marchenko.news.exception.ServiceException;
import com.marchenko.news.service.interfaces.CommentAction;
import com.marchenko.news.service.interfaces.NewsAction;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

/**
 * Created by Marchenko Vadim on 2/26/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/test/resources/test_spring.xml"})
public class NewsPostActionImplTest {
    @Mock
    private CommentAction commentAction;
    @Mock
    private NewsAction newsAction;
    @InjectMocks
    private NewsPostActionImpl newsPostAction;

    private NewsPost newsPost;
    private List<Tag> tags;
    private Author author;
    private News news;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        newsPost = new NewsPost();
        tags = new ArrayList<>();
        author = new Author("author1");
        news = new News("title", "fullText");
        Tag tag1 = new Tag("tag1");
        tag1.setId(1L);
        Tag tag2 = new Tag("tag2");
        tag2.setId(2L);
        tags.add(tag1);
        tags.add(tag2);
        author.setId(1L);
        newsPost.setAuthor(author);
        newsPost.setTags(tags);
        newsPost.setNews(news);
    }

    @Test
    public void addNews() throws ServiceException, DaoException {
        Optional<Long> one = Optional.of(1L);
        when(newsAction.create(news)).thenReturn(one);
        doThrow(ServiceException.class).when(newsAction).attachAuthor(anyLong(), eq(2L));
        newsPostAction.createNewsPost(newsPost);
        verify(newsAction).attachTag(1L, 1L);
        verify(newsAction).attachTag(1L, 2L);
        verify(newsAction).attachAuthor(1L, 1L);
        verify(newsAction).create(news);
    }

    @Test
    public void editNews() throws ServiceException, DaoException {
        newsPostAction.editNewsPostContent(newsPost);
        verify(newsAction).update(news);
        verify(newsAction).detachAllAuthors(news.getId());
        verify(newsAction).detachAllTags(news.getId());
        verify(newsAction).attachTag(news.getId(), tags.get(0).getId());
        verify(newsAction).attachTag(news.getId(), tags.get(1).getId());
        verify(newsAction).attachAuthor(news.getId(), author.getId());
    }

    @Test
    public void deleteNews() throws ServiceException, DaoException {
        newsPostAction.deleteNewsMessage(1L);
        verify(newsAction).detachAllAuthors(1L);
        verify(newsAction).detachAllTags(1L);
        verify(commentAction).deleteByNewsId(1L);
    }

    @Test
    public void findNews() throws ServiceException, DaoException {
        List<Author> authors = new ArrayList<>();
        List<Comment> comments = new ArrayList<>();
        Comment comment = new Comment("comment 1");
        comments.add(comment);
        authors.add(author);
        when(newsAction.read(1L)).thenReturn(Optional.of(news));
        when(newsAction.findAttachedAuthors(1L)).thenReturn(authors);
        when(newsAction.findAttachedTags(1L)).thenReturn(tags);
        when(commentAction.findByNewsId(1L)).thenReturn(comments);

        NewsPost message = newsPostAction.readNews(1L).get();

        verify(newsAction).read(1L);
        verify(newsAction).findAttachedAuthors(1L);
        verify(newsAction).findAttachedTags(1L);
        verify(commentAction).findByNewsId(1L);

        Assert.assertEquals(comments, message.getComments());
        Assert.assertEquals(news, message.getNews());
        Assert.assertEquals(tags, message.getTags());
        Assert.assertEquals(author, message.getAuthor());
    }
}
