package com.marchenko.news.service.impl;

import com.marchenko.news.exception.DaoException;
import com.marchenko.news.dao.interfaces.AuthorDao;
import com.marchenko.news.entity.Author;
import com.marchenko.news.exception.ServiceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static org.mockito.Mockito.*;


/**
 * Created by Marchenko Vadim on 2/26/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/test/resources/test_spring.xml"})
public class AuthorActionImplTest {
    @Mock
    private AuthorDao authorDao;
    @InjectMocks
    private AuthorActionImpl authorAction;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createAuthor() throws DaoException, ServiceException {
        Author author = new Author("Asd");
        when(authorDao.create(any())).thenReturn(Optional.of(1L));
        authorAction.createAuthor(author);
        Assert.assertEquals(Long.valueOf(1L), author.getId());
        author.setId(null);
    }
}
