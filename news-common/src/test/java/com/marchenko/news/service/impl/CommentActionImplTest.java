package com.marchenko.news.service.impl;

import com.marchenko.news.exception.DaoException;
import com.marchenko.news.dao.interfaces.CommentDao;
import com.marchenko.news.entity.Comment;
import com.marchenko.news.exception.ServiceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static org.mockito.Mockito.*;

/**
 * Created by Marchenko Vadim on 2/26/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/test/resources/test_spring.xml"})
public class CommentActionImplTest {
    @Mock
    private CommentDao commentDao;
    @InjectMocks
    private CommentActionImpl defaultCommentAction;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addComment() throws DaoException, ServiceException {
        Comment comment = new Comment("1212");
        when(commentDao.create(comment)).thenReturn(Optional.of(1L));
        defaultCommentAction.create(comment);
        Assert.assertEquals(comment.getId(), Long.valueOf(1));
        Assert.assertNotNull(comment.getCreationDate());
        comment.setId(null);
        comment.setCreationDate(null);
        verify(commentDao).create(comment);
    }

    @Test
    public void removeComment() throws DaoException, ServiceException {
        defaultCommentAction.delete(anyLong());
        verify(commentDao).delete(anyLong());
    }
}
