package com.marchenko.news.service.impl;

import com.marchenko.news.dao.interfaces.UserDao;
import com.marchenko.news.entity.User;
import com.marchenko.news.exception.DaoException;
import com.marchenko.news.exception.ServiceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static org.mockito.Mockito.*;

/**
 * Created by Marchenko Vadim on 4/6/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/test/resources/test_spring.xml"})
public class UserActionImplTest {
    @Mock
    private UserDao userDao;
    @InjectMocks
    private UserActionImpl userAction;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void readUser() throws DaoException, ServiceException {
        User user = new User();
        user.setName("name");
        when(userDao.read(1L)).thenReturn(Optional.of(user));
        when(userDao.read(2L)).thenReturn(Optional.empty());
        Optional<User> optional1 = userAction.readUser(1L);
        Assert.assertEquals(user, optional1.get());
        Optional<User> optional2 = userAction.readUser(2L);
        Assert.assertFalse(optional2.isPresent());
    }
}
