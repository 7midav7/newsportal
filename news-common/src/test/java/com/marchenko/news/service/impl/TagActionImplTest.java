package com.marchenko.news.service.impl;


import com.marchenko.news.exception.DaoException;
import com.marchenko.news.dao.interfaces.TagDao;
import com.marchenko.news.entity.Tag;
import com.marchenko.news.exception.ServiceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static org.mockito.Mockito.*;

/**
 * Created by Marchenko Vadim on 2/26/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/test/resources/test_spring.xml"})
public class TagActionImplTest {
    @Mock
    private TagDao tagDao;
    @InjectMocks
    private TagActionImpl defaultTagAction;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void removeTag() throws ServiceException, DaoException {
        defaultTagAction.deleteTag(1L);
        verify(tagDao).delete(1L);
    }

    @Test
    public void createTag() throws ServiceException, DaoException {
        Tag tag = new Tag("tag");
        when(tagDao.create(tag)).thenReturn(Optional.of(1L));
        defaultTagAction.createTag(tag);
        Assert.assertEquals(tag.getId(), Long.valueOf(1));
    }
}
