--------------------------------------------------------
--  DDL for Sequence AUTH_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "AUTH_SEQ"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 243 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence COMM_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "COMM_SEQ"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 180 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence NEWS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "NEWS_SEQ"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 232 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence TAGS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "TAGS_SEQ"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 246 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table USRS_SEQ
--------------------------------------------------------
   CREATE SEQUENCE  "USRS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table AUTHORS

  CREATE TABLE "AUTHORS"
   (	"AUTH_ID" NUMBER(20,0), 
	"AUTH_NAME" NVARCHAR2(30), 
	"AUTH_EXPIRED" TIMESTAMP (6)
   );
--------------------------------------------------------
--  DDL for Table AUTHOR_NEWS
--------------------------------------------------------

  CREATE TABLE "AUTHOR_NEWS"
   (	"AUNE_NEWS_ID" NUMBER(20,0), 
	"AUNE_AUTHOR_ID" NUMBER(20,0)
   );
--------------------------------------------------------
--  DDL for Table COMMENTS
--------------------------------------------------------

  CREATE TABLE "COMMENTS"
   (	"COMM_ID" NUMBER(20,0), 
	"COMM_NEWS_ID" NUMBER(20,0), 
	"COMM_TEXT" NVARCHAR2(100), 
	"COMM_CREATION_DATE" TIMESTAMP (6)
   );
--------------------------------------------------------
--  DDL for Table NEWS
--------------------------------------------------------

  CREATE TABLE "NEWS"
   (	"NEWS_ID" NUMBER(20,0), 
	"NEWS_TITLE" NVARCHAR2(30), 
	"NEWS_SHORT_TEXT" NVARCHAR2(100), 
	"NEWS_FULL_TEXT" NVARCHAR2(2000), 
	"NEWS_CREATION_DATE" TIMESTAMP (6), 
	"NEWS_MODIFICATION_DATE" DATE
   );
--------------------------------------------------------
--  DDL for Table NEWS_TAGS
--------------------------------------------------------

  CREATE TABLE "NEWS_TAGS"
   (	"NETA_NEWS_ID" NUMBER(20,0), 
	"NETA_TAG_ID" NUMBER(20,0)
   );
--------------------------------------------------------
--  DDL for Table ROLES
--------------------------------------------------------

  CREATE TABLE "ROLES"
   (	"ROLS_USER_ID" NUMBER(20,0), 
	"ROLS_NAME" VARCHAR2(50 BYTE)
   );
--------------------------------------------------------
--  DDL for Table TAGS
--------------------------------------------------------

  CREATE TABLE "TAGS"
   (	"TAGS_ID" NUMBER(20,0), 
	"TAGS_NAME" VARCHAR2(30 BYTE)
   );
--------------------------------------------------------
--  DDL for Table USERS
--------------------------------------------------------

  CREATE TABLE "USERS"
   (	"USRS_ID" NUMBER(20,0), 
	"USRS_NAME" NVARCHAR2(50), 
	"USRS_LOGIN" VARCHAR2(30 BYTE), 
	"USRS_PASSWORD" VARCHAR2(30 BYTE)
   );
  --------------------------------------------------------
--  DDL for Index AUTHORS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "AUTHORS_PK" ON "AUTHORS" ("AUTH_ID");
--------------------------------------------------------
--  DDL for Index NEWS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "NEWS_PK" ON "NEWS" ("NEWS_ID");
--------------------------------------------------------
--  DDL for Index COMMENTS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "COMMENTS_PK" ON "COMMENTS" ("COMM_ID");
--------------------------------------------------------
--  DDL for Index USERS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "USERS_PK" ON "USERS" ("USRS_ID");
--------------------------------------------------------
--  DDL for Index TAGS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "TAGS_PK" ON "TAGS" ("TAGS_ID");
--------------------------------------------------------
--  Constraints for Table AUTHORS
--------------------------------------------------------

  ALTER TABLE "AUTHORS" ADD CONSTRAINT "AUTHORS_PK" PRIMARY KEY ("AUTH_ID");
  ALTER TABLE "AUTHORS" MODIFY ("AUTH_NAME" NOT NULL ENABLE);
  ALTER TABLE "AUTHORS" MODIFY ("AUTH_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table USERS
--------------------------------------------------------

  ALTER TABLE "USERS" ADD CONSTRAINT "USERS_PK" PRIMARY KEY ("USRS_ID");
  ALTER TABLE "USERS" MODIFY ("USRS_ID" NOT NULL ENABLE);
  ALTER TABLE "USERS" MODIFY ("USRS_PASSWORD" NOT NULL ENABLE);
  ALTER TABLE "USERS" MODIFY ("USRS_LOGIN" NOT NULL ENABLE);
  ALTER TABLE "USERS" MODIFY ("USRS_NAME" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NEWS_TAGS
--------------------------------------------------------

  ALTER TABLE "NEWS_TAGS" MODIFY ("NETA_TAG_ID" NOT NULL ENABLE);
  ALTER TABLE "NEWS_TAGS" MODIFY ("NETA_NEWS_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TAGS
--------------------------------------------------------

  ALTER TABLE "TAGS" ADD CONSTRAINT "TAGS_PK" PRIMARY KEY ("TAGS_ID");
  ALTER TABLE "TAGS" MODIFY ("TAGS_NAME" NOT NULL ENABLE);
  ALTER TABLE "TAGS" MODIFY ("TAGS_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table COMMENTS
--------------------------------------------------------

  ALTER TABLE "COMMENTS" MODIFY ("COMM_ID" NOT NULL ENABLE);
  ALTER TABLE "COMMENTS" MODIFY ("COMM_CREATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "COMMENTS" MODIFY ("COMM_TEXT" NOT NULL ENABLE);
  ALTER TABLE "COMMENTS" MODIFY ("COMM_NEWS_ID" NOT NULL ENABLE);
  ALTER TABLE "COMMENTS" ADD CONSTRAINT "COMMENTS_PK" PRIMARY KEY ("COMM_ID");
--------------------------------------------------------
--  Constraints for Table AUTHOR_NEWS
--------------------------------------------------------

  ALTER TABLE "AUTHOR_NEWS" MODIFY ("AUNE_AUTHOR_ID" NOT NULL ENABLE);
  ALTER TABLE "AUTHOR_NEWS" MODIFY ("AUNE_NEWS_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ROLES
--------------------------------------------------------

  ALTER TABLE "ROLES" MODIFY ("ROLS_NAME" NOT NULL ENABLE);
  ALTER TABLE "ROLES" MODIFY ("ROLS_USER_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NEWS
--------------------------------------------------------

  ALTER TABLE "NEWS" MODIFY ("NEWS_CREATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "NEWS" MODIFY ("NEWS_MODIFICATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "NEWS" MODIFY ("NEWS_FULL_TEXT" NOT NULL ENABLE);
  ALTER TABLE "NEWS" MODIFY ("NEWS_SHORT_TEXT" NOT NULL ENABLE);
  ALTER TABLE "NEWS" MODIFY ("NEWS_TITLE" NOT NULL ENABLE);
  ALTER TABLE "NEWS" MODIFY ("NEWS_ID" NOT NULL ENABLE);
  ALTER TABLE "NEWS" ADD CONSTRAINT "NEWS_PK" PRIMARY KEY ("NEWS_ID");
--------------------------------------------------------
--  Ref Constraints for Table AUTHOR_NEWS
--------------------------------------------------------

  ALTER TABLE "AUTHOR_NEWS" ADD CONSTRAINT "AUNE_AUTH_AUTHOR_ID" FOREIGN KEY ("AUNE_AUTHOR_ID")
    REFERENCES "AUTHORS" ("AUTH_ID") ENABLE;
  ALTER TABLE "AUTHOR_NEWS" ADD CONSTRAINT "AUNE_NEWS_NEWS_ID" FOREIGN KEY ("AUNE_NEWS_ID")
    REFERENCES "NEWS" ("NEWS_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table COMMENTS
--------------------------------------------------------

  ALTER TABLE "COMMENTS" ADD CONSTRAINT "COMM_NEWS_NEWS_ID" FOREIGN KEY ("COMM_NEWS_ID")
    REFERENCES "NEWS" ("NEWS_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table NEWS_TAGS
--------------------------------------------------------

  ALTER TABLE "NEWS_TAGS" ADD CONSTRAINT "NETA_NEWS_NEWS_ID" FOREIGN KEY ("NETA_NEWS_ID")
    REFERENCES "NEWS" ("NEWS_ID") ENABLE;
  ALTER TABLE "NEWS_TAGS" ADD CONSTRAINT "NETA_TAGS_TAG_ID" FOREIGN KEY ("NETA_TAG_ID")
    REFERENCES "TAGS" ("TAGS_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table ROLES
--------------------------------------------------------

  ALTER TABLE "ROLES" ADD CONSTRAINT "ROLES_FK1" FOREIGN KEY ("ROLS_USER_ID")
    REFERENCES "USERS" ("USRS_ID") ENABLE;
--------------------------------------------------------
--  DDL for Trigger AUTH_TRIGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "AUTH_TRIGER"
BEFORE INSERT ON AUTHORS
FOR EACH ROW
 WHEN (NEW.auth_id is NULL) BEGIN
  SELECT auth_seq.nextval INTO :new.auth_id FROM DUAL;
END;
/
ALTER TRIGGER "AUTH_TRIGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger COMM_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "COMM_TRG"
   BEFORE INSERT ON COMMENTS
   FOR EACH ROW
    WHEN (NEW.comm_id IS NULL) BEGIN
   SELECT comm_seq.NEXTVAL INTO :NEW.comm_id FROM DUAL;
END;
/
ALTER TRIGGER "COMM_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger NEWS_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "NEWS_TRG"
   BEFORE INSERT ON news
   FOR EACH ROW
    WHEN (NEW.news_id IS NULL) BEGIN
   SELECT news_seq.NEXTVAL INTO :NEW.news_id FROM DUAL;
END;
/
ALTER TRIGGER "NEWS_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TAGS_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "TAGS_TRG"
   BEFORE INSERT ON TAGS
   FOR EACH ROW
    WHEN (NEW.tags_id IS NULL) BEGIN
   SELECT tags_seq.NEXTVAL INTO :NEW.tags_id FROM DUAL;
END;
/
ALTER TRIGGER "TAGS_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger USRS_TRIGER
--------------------------------------------------------

CREATE OR REPLACE TRIGGER "USRS_TRIGER"
BEFORE INSERT ON USERS
FOR EACH ROW
  WHEN (NEW.usrs_id is NULL) BEGIN
  SELECT usrs_seq.nextval INTO :new.usrs_id FROM DUAL;
END;
/
ALTER TRIGGER "USRS_TRIGER" ENABLE;